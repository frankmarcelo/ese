<?php
/**
 * File: kernel.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  Kernel
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
use Silex\Provider;
use Bdt\Clickatell\ClickatellClient;
use DerAlex\Silex\YamlConfigServiceProvider;
use Ese\Models\Booking;
use Ese\Models\GeneralInfo;
use Ese\Exception;
use Ese\ServiceProvider\EseServiceProvider;
use SimpleUser\UserManager;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Knp\Provider\ConsoleServiceProvider;
use Symfony\Bridge\Doctrine\Logger\DbalLogger;

$app->register(new Provider\SecurityServiceProvider());
$app->register(new Provider\RememberMeServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());
$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(new Provider\TwigServiceProvider());
$app->register(new Provider\SwiftmailerServiceProvider());
$app->register(new Provider\FormServiceProvider());
$app->register(new Provider\HttpCacheServiceProvider());
$app->register(new Provider\ValidatorServiceProvider());
$app->register(new YamlConfigServiceProvider(__DIR__ . '/config/parameters.yml'));
$app->register(new SimpleUser\UserServiceProvider());

/**
 * Set the calling of $app['config']['CONFIG_URIS'] to shorter array key hash
 */
$app['parameters'] = $app->share(
    function () use ($app) {
        return $app['config']['parameters'];
    }
);

/**
 * Address finder api key and secret
 */
$app['addressfinder'] = $app->share(
    function () use ($app) {

        if (isset($_ENV['ADDRESS_FINDER_KEY'])) {
            return array(
                'key' => $_ENV['ADDRESS_FINDER_KEY'],
                'secret' => $_ENV['ADDRESS_FINDER_SECRET'],
            );
        } else {
            return array(
                'key' => $app['config']['addressfinder']['key'],
                'secret' => $app['config']['addressfinder']['secret'],
            );
        }
    }
);

/**
 * Recaptcha service provider
 * Set the calling of $app['config']['api'] urls depending on development
 * environment to shorter array key hash
 */
$app['captcha'] = $app->share(
    function () use ($app) {

        if (isset($_ENV['RECAPTCHA_KEY'])) {
            return array(
                'key' => $_ENV['RECAPTCHA_KEY'],
                'secret' => $_ENV['RECAPTCHA_SECRET'],
            );
        } else {
            return array(
                'key' => $app['config']['recaptcha']['key'],
                'secret' => $app['config']['recaptcha']['secret'],
            );
        }
    }
);

/**
 * Set to determine if the current environment is production or development
 */
$app['debug'] = $app['parameters']['environment.debug'];

/**
 * Doctrine service provider
 */
$app['db.options'] = $app->share(
    function () use ($app) {

        if (isset($_ENV['CLEARDB_DATABASE_URL'])) {
            echo 'frnak';die;
            return array(
                'driver' => $_ENV['CLEARDB_DATABASE_DRIVER'],
                'host' => $_ENV['CLEARDB_DATABASE_HOST'],
                'dbname' => $_ENV['CLEARDB_DATABASE_NAME'],
                'user' => $_ENV['CLEARDB_DATABASE_USER'],
                'password' => $_ENV['CLEARDB_DATABASE_PASS']
            );

        } else {
            return array(
                'driver' => $app['config']['db']['DB_DRIVER'],
                'host' => $app['config']['db']['DB_HOST'],
                'dbname' => $app['config']['db']['DB_NAME'],
                'user' => $app['config']['db']['DB_USER'],
                'password' => $app['config']['db']['DB_PASS']
            );
        }
    }
);

$app->register(
    new Provider\DoctrineServiceProvider(),
    array
    (
        'db.options' => array
        (
            'driver' => $app['db.options']['driver'],
            'host' => $app['db.options']['host'],
            'dbname' => $app['db.options']['dbname'],
            'user' => $app['db.options']['user'],
            'password' => $app['db.options']['password']
        )
    )
);

/**
 * Session service provider
 */
$app['session.db_options'] = array(
    'db_table' => 'session',
    'db_id_col' => 'session_id',
    'db_data_col' => 'session_value',
    'db_time_col' => 'session_time',
);

$app['session.storage.handler'] = $app->share(
    function () use ($app) {
        return new PdoSessionHandler(
            $app['db']->getWrappedConnection(),
            $app['session.db_options'],
            $app['session.storage.options']
        );
    }
);

/**
 * Set base directory for saving file in webroot directory
 */
$app['directory.base'] = $app->share(
    function () use ($app) {
        return dirname(dirname(__FILE__));
    }
);

/**
 * Cache settings for storing badges value
 */
$app['cache'] = $app->share(
    function () use ($app) {
        return array(
            'directory' => '/' . $app['parameters']['cache.directory'],
            'lifetime' => $app['parameters']['cache.lifetime'],
            'key' => $app['parameters']['cache.key']
        );
    }
);

/**
 * Twig service provider set the variables for twigs or template engine.
 */
$app->register(
    new Provider\TwigServiceProvider(),
    array(
        'twig.options' => array(
            'cache' => $app['cache']['directory'] . '/twigs',
            'strict_variables' => true,
            'debug' => $app['debug'],
            'auto_reload' => $app['debug'],
        ),
        'twig.path' => array(__DIR__ . '/views')
    )
);

/**
 * Monolog service provider
 * Logging facility for the entire application the log level can be found.
 */
if (!$app['debug']) {

    $app->register(
        new Provider\MonologServiceProvider(),
        array (
            'monolog.logfile' => 'php://stderr',
        )
    );

} else {

    $app->register(
        new Provider\MonologServiceProvider(),
        array(
            'monolog.logfile' => __DIR__ . '/logs/app.log',
            'monolog.name' => 'app',
            'monolog.level' =>
            Logger::getLevels()[strtoupper($app['parameters']['monolog_level'])]
        )
    );

    /**
     * Extend the fingerscrossedhandler for monolog
     */
    $app['monolog'] = $app->share(
        $app->extend(
            'monolog',
            function ($monolog, $app) {
                $monolog->pushHandler(
                    new Monolog\Handler\FingersCrossedHandler(
                        $app['monolog.handler'],
                        Logger::getLevels()[
                        strtoupper($app['parameters']['monolog_action_level'])
                        ],
                        0,
                        false
                    )
                );

                /**
                 * Display the debug option and enabled the print out log
                 * on FirePHP so we can see the logs happening in the background.
                 * As well as display web profiler if debug option is enabled.
                 */
                if ($app['debug']) {
                    $monolog->pushHandler(new Monolog\Handler\FirePHPHandler());
                }

                return $monolog;
            }
        )
    );

    /**
     * Web Profiler service provider
     * Display the web application profiler or not,
     * this is useful for developer that's working on local environment.
     * It helps to determine the statistics of page load and performance.
     */
    $app->register(
        new Provider\WebProfilerServiceProvider(),
        array(
            'profiler.cache_dir' => $app['cache']['directory'] . '/profiler',
        )
    );

}

if ($app['debug']) {
    $logger = new Doctrine\DBAL\Logging\DebugStack();
    $app['db.config']->setSQLLogger($logger);
    $app->error(function(\Exception $e, $code) use ($app, $logger) {
        if ( $e instanceof PDOException and count($logger->queries) ) {
            // We want to log the query as an ERROR for PDO exceptions!
            $query = array_pop($logger->queries);
            $app['monolog']->err($query['sql'], array(
                'params' => $query['params'],
                'types' => $query['types']
            ));
        }
    });
    $app->after(function() use ($app, $logger) {
        foreach ($logger->queries as $query) {
            $app['monolog']->addInfo($query['sql'], array(
                'params' => $query['params'],
                'types' => $query['types']
            ));
        }
    });
}

/**
 * Set the calling of $app['config']['api'] urls depending on development
 * environment to shorter array key hash
 */
$app['clickatell'] = $app->share(
    function () use ($app) {
        if (isset($_ENV['CLICKATELL_API_ID'])) {
            return array(
                'CLICKATELL_API_ID' => $_ENV['CLICKATELL_API_ID'],
                'CLICKATELL_USER' => $_ENV['CLICKATELL_USER'],
                'CLICKATELL_PASSWORD' => $_ENV['CLICKATELL_PASSWORD'],
                'CLICKATELL_MOBILE' => $_ENV['CLICKATELL_MOBILE']
            );
        } else {
            return $app['config']['api']['clickatell'];
        }
    }
);

/**
 * Sms clickatell client service provider
 */
$app['sms.request'] = $app->share(
    function () use ($app) {
        try {
            return ClickatellClient::factory(
                array (
                    'api_id' => $app['clickatell']["CLICKATELL_API_ID"],
                    'user' => $app['clickatell']['CLICKATELL_USER'],
                    'password' => $app['clickatell']['CLICKATELL_PASSWORD']
                )
            );

        } catch (Exception\RuntimeException $e) {
            $app['monolog']->addInfo($e->getMessage());
            throw $e;
        }
    }
);

/**
 * Swiftmailer service provider
 */
$app['swiftmailer.options'] = $app->share(
    function () use ($app) {

        if (isset($_ENV['SWIFTMAILER_HOST'])) {

            return array(
                'host' => $_ENV['SWIFTMAILER_HOST'],
                'port' => $_ENV['SWIFTMAILER_PORT'],
                'username' => $_ENV['SWIFTMAILER_USERNAME'],
                'password' => $_ENV['SWIFTMAILER_PASSWORD'],
                'encryption' => $_ENV['SWIFTMAILER_ENCRYPTION'],
                'auth_mode' => $_ENV['SWIFTMAILER_AUTH_MODE'],
                'bcc' => $_ENV['SWIFTMAILER_BCC']
            );

        } else {

            return array(
                'host' => $app['config']['swiftmailer']['host'],
                'port' => $app['config']['swiftmailer']['port'],
                'username' => $app['config']['swiftmailer']['username'],
                'password' => $app['config']['swiftmailer']['password'],
                'encryption' => $app['config']['swiftmailer']['encryption'],
                'auth_mode' => $app['config']['swiftmailer']['auth_mode'],
                'bcc' => $app['config']['swiftmailer']['bcc']
            );
        }
    }
);

$app['user.options'] = array(
    'templates' => array(
        'layout' => 'layout.twig',
        'view' => 'view.twig',
        'register' => 'register.twig',
        'edit' => 'edit.twig',
        'login' => 'login.twig',
        'list' => 'list.twig',
    ),
    'mailer' => array(
        'enabled' => true,
        'fromEmail' => array(
            'address' => $app['swiftmailer.options']['username'],
            'name' => $app['parameters']['default.title.home_page']
        ),
    ),
    'emailConfirmation' => array(
        'required' => true,
        'template' => '@user/email/confirm-email.twig',
    ),
    'passwordReset' => array(
        'template' => '@user/email/reset-password.twig',
        'tokenTTL' => 86400
    ),
    'isUsernameRequired' => true,
    'userClass' => '\Ese\Users\User'
);

$app['security.firewalls'] = array(
    'secured_area' => array(
        'pattern' => '^.*$',
        'anonymous' => true,
        'remember_me' => array(),
        'form' => array(
            'login_path' => '/user/login',
            'check_path' => '/user/login_check',
            'csrf_provider' => 'form.csrf_provider'
        ),
        'logout' => array(
            'logout_path' => '/user/logout',
        ),
        'users' => $app->share(
            function ($app) {
                return $app['user.manager'];
            }
        )
    )
);

/**
 * Example of defining a custom password strength validator.
 * Must return an error string on failure, or null on success.
 */
$app['user.passwordStrengthValidator'] = $app->protect(
    function (SimpleUser\User $user, $password) {

        if (strlen($password) < 4) {
            return 'Password must be at least 4 characters long.';
        }
        if (strtolower($password) == strtolower($user->getName())) {
            return 'Your password cannot be the same as your name.';
        }
    }
);

$app['dispatcher']->addListener(
    SimpleUser\UserEvents::AFTER_INSERT,
    function (SimpleUser\UserEvent $event) use ($app) {

        $user = $event->getUser();
        $statement = $app['db']->executeQuery(
            'SELECT * FROM locations WHERE name = ?',
            array($user->getSuburb())
        );

        $locations = $statement->fetchAll();

        if (!$locations) {
            $app['monolog']->addInfo(
                'Created user locations is not found - ' .
                $user->getAddress()
            );
        }

        $sql = 'UPDATE users
                SET address = :address
                , mobile = :mobile
                , suburb = :suburb
                , geocode_x = :geocode_x
                , geocode_y = :geocode_y
                , location_id = :location_id
                WHERE id = :id';

        $params = array(
            'address' => $user->getAddress(),
            'mobile' => $user->getMobile(),
            'suburb' => $user->getSuburb(),
            'geocode_x' => $user->getGeocodeX(),
            'geocode_y' => $user->getGeocodeY(),
            'location_id' => $locations[0]['id'],
            'id' => $user->getId(),
        );

        $app['db']->executeUpdate($sql, $params);
        $generalInfo = new GeneralInfo($app);
        $info = $generalInfo->findById(1);
        $generalInfo->updateGeneralInfoById(
            array(
                'total_users' => $info->getTotalUsers() + 1,
            ),
            array(
                'id' => $info->getId()
            )
        );
    }
);

/**
 * Console Service Provider
 *
 */
$app->register(new ConsoleServiceProvider(), array(
    'console.name' => 'ConsoleApp',
    'console.version' => '1.0.0',
    'console.project_directory' => __DIR__ . '/..'
));

$app['dispatcher']->addListener(
    SimpleUser\UserEvents::AFTER_UPDATE,
    function (SimpleUser\UserEvent $event) use ($app) {

        $user = $event->getUser();
        $statement = $app['db']->executeQuery(
            'SELECT * FROM locations WHERE name = ?',
            array(
                $user->getSuburb()
            )
        );

        $locations = $statement->fetchAll();
        if (!$locations) {
            $app['monolog']->addInfo(
                'Created user locations is not found - ' .
                $user->getAddress()
            );
        }

        $sql = 'UPDATE users
                SET address = :address
                , mobile = :mobile
                , suburb = :suburb
                , geocode_x = :geocode_x
                , geocode_y = :geocode_y
                , location_id = :location_id
                WHERE id = :id';

        $params = array(
            'address' => $user->getAddress(),
            'mobile' => $user->getMobile(),
            'suburb' => $user->getSuburb(),
            'geocode_x' => $user->getGeocodeX(),
            'geocode_y' => $user->getGeocodeY(),
            'location_id' => $locations[0]['id'],
            'id' => $user->getId(),
        );

        $app['db']->executeUpdate($sql, $params);
    }
);

$app['twig'] = $app->share(
    $app->extend(
        'twig',
        function (\Twig_Environment $twig, $app) {

            $booking = new Booking($app);
            $users = new UserManager($app['db'], $app);
            $twig->addGlobal('analytics', $app['parameters']['google.analytics']);
            $twig->addGlobal('title', $app['parameters']['default.title.home_page']);
            $twig->addGlobal(
                'branding',
                $app['parameters']['default.title.home_page']
            );
            $twig->addGlobal('recaptcha_key', $app['captcha']['key']);
            $twig->addGlobal('recaptcha_secret', $app['captcha']['secret']);
            $twig->addGlobal('address_finder_key', $app['addressfinder']['key']);
            $twig->addGlobal(
                'address_finder_secret',
                $app['addressfinder']['secret']
            );
            $twig->addGlobal('latest_bookings', $booking->findAllLatestBookings());
            $twig->addGlobal('latest_users', $users->findAllLatestUsers());

            $twig->addExtension(new \Ese\Twig\Elasped($app));
            $twig->addExtension(new \Ese\Twig\Gravatar($app));
            $twig->addExtension(new \Ese\Twig\Msisdn($app));
            $twig->addExtension(new \Ese\Twig\Wrap($app));
            $twig->addExtension(new \Twig_Extensions_Extension_Text($app));

            return $twig;
        }
    )
);

$app->register(new EseServiceProvider());

return $app;

#!/usr/bin/env php
<?php

set_time_limit(0);
require_once __DIR__.'../../../vendor/autoload.php';

$app = new Silex\Application();
$app = require_once __DIR__. '../../kernel.php';

use Ese\Console\Command\UpdateMonthlyInfoCommand;

$application = $app['console'];
$application->add(new UpdateMonthlyInfoCommand($app));
$application->run();
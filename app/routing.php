<?php
/**
 * File: routing.php
 *
 * PHP version 5.4
 *
 * @category Routing
 * @package  Routing
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Ese\Controllers\EseControllerProvider;

$app['monolog']->addInfo('bootstrap routing');

/**
 * Mount or register the ese controller provider,
 * where all routing definitions are defined.
 */
$app->mount('/', new EseControllerProvider());
$app->mount('/user', new SimpleUser\UserServiceProvider());

/**
 * Before middleware
 */
$app->before(
    function (Request $request) use ($app) {
        $token = $app['security']->getToken();
        $app['user'] = null;

        if ($token
            && !$app['security.trust_resolver']->isAnonymous($token)
        ) {
            $app['user'] = $token->getUser();
        }

        $responseHeader = $request->headers->get('Content-Type');
        if (0 === strpos($responseHeader, 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
    }
);

/**
 * After middleware
 * Fix the issues with back history button.
 */
$app->after(
    function (Request $request, Response $response) {
        $response->setMaxAge(0);
        $response->setPrivate();
        $response->setExpires();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('max-age', 0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);
    }
);

/**
 * Catch all not found or exceptions
 */
$app->error(
    function (\Exception $e, $code) use ($app) {
        if ($app['debug']) {
            return;
        }

        switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
        }

        $app['monolog']->addError($message . ' (' . $code . ')');
        $response = new Response($app['twig']->render('404.twig'));
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        $response->setTtl($app['parameters']['http.cache_expiration']);

        return $response;
    }
);

return $app;
What is Project ESE?
- Providing Excellent Service Efficiently 

Why is it needed?
- To address opportunities for improvement in current operations 
- Maximize profitability 

What do we want to achieve? 
- Increase in productivity (measured by the number of box pick up and drop offs)
- Increase in efficiency   (measured by box pick up and drop offs VS time spent)
- Decrease in OPEX (measured by fuel and time spent per run)
- Increase in Profit (measured by net sales)

Brainstorming 1.0
- Online registration and form (we need to make this as user friendly as possible)
- Username and password
- Calendar selection for pick up and drop offs (suburb segmented)
- Extra fee for pick up and drop offs outside suburb schedule
- SMS confirmation upon generation of journey plan based on pick up and drop off bookings
- Promo roll-out to entice customers to register online 

Action needed:
- Ese to give list of suburbs segmented per run/day. 
- Ese to provide historical figures for benchmarking 

    Average # of pick up and drop off per run
    Average kms per run
    Average gas consumption 
    Average time spent per customer per run
<?php
/**
 * File: Location.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class Location
 * This script is the main routing interface to bootstrap
 *
 * Class Location
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Location implements \Serializable
{
    private $app;
    protected $id;
    protected $zoneId;
    protected $name;
    protected $lft;
    protected $rgt;
    protected $level;
    protected $displayAsOption;
    protected $hasChildren;
    protected $pickUpExtraCharge;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $zoneId
     * @return void
     */
    public function setZoneId($zoneId)
    {
        $this->zoneId = $zoneId;
    }

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * @param $lft
     * @return void
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param $rgt
     * @return void
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param $level
     * @return void
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param $displayAsOption
     * @return void
     */
    public function setDisplayAsOption($displayAsOption)
    {
        $this->displayAsOption = $displayAsOption;
    }

    /**
     * @return mixed
     */
    public function getDisplayAsOption()
    {
        return $this->displayAsOption;
    }

    /**
     * @param $hasChildren
     * @return void
     */
    public function setHasChildren($hasChildren)
    {
        $this->hasChildren = $hasChildren;
    }

    /**
     * @return mixed
     */
    public function getHasChildren()
    {
        return $this->hasChildren;
    }

    /**
     * @param $pickUpExtraCharge
     * @return void
     */
    public function setPickUpExtraCharge($pickUpExtraCharge)
    {
        $this->pickUpExtraCharge = $pickUpExtraCharge;
    }

    /**
     * @return mixed
     */
    public function getPickUpExtraCharge()
    {
        return $this->pickUpExtraCharge;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * @param array $data
     * @return Location
     */
    protected function hydrate(array $data)
    {
        $location = new Location($this->app);
        $location->setId($data['id']);
        $location->setZoneId($data['zone_id']);
        $location->setName($data['name']);
        $location->setLft($data['lft']);
        $location->setRgt($data['rgt']);
        $location->setLevel($data['level']);
        $location->setDisplayAsOption($data['display_as_option']);
        $location->setHasChildren($data['has_children']);
        $location->setPickUpExtraCharge($data['pick_up_extra_charge']);
        $location->setDteCreated($data['dte_created']);

        return $location;
    }

    public function findAllLocationWithExtraCharges()
    {
        $sql = '
            SELECT *
              FROM locations
                  WHERE pick_up_extra_charge > 0
                    ORDER BY name';

        $data = $this->app['db']->fetchAll($sql);
        $locations = array();
        foreach ($data as $info)
        {
            $locations[] = $this->hydrate($info);
        }

        return $locations;
    }

    /**
     * @param $locationId
     * @return bool|Location
     */
    public function findById($locationId)
    {
        $sql = '
            SELECT *
              FROM locations
                  WHERE id =:location_id';

        $params = array(
            'location_id' => $locationId,
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        if ($data) {
            return $this->hydrate($data[0]);
        }

        return false;
    }

    /**
     * @param $userId
     * @return bool|Location
     */
    public function findByUserId($userId)
    {
        $sql = '
            SELECT l.*
              FROM locations l
                  JOIN users u ON (u.location_id = l.id)
                      WHERE u.id = :user_id';

        $params = array(
            'user_id' => $userId,
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        if ($data) {
            return $this->hydrate($data[0]);
        }

        return false;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: BoxType.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class BoxType
 * This script is the main routing interface to bootstrap
 *
 * Class BoxType
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class BoxType implements \Serializable
{
    private $app;
    protected $id;
    protected $description;
    protected $dimensions;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $dimensions
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    /**
     * @return mixed
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param array $data
     * @return BoxPricing
     */
    protected function hydrate(array $data)
    {
        $boxType = new BoxType($this->app);
        $boxType->setId($data['id']);
        $boxType->setDescription($data['description']);
        $boxType->setDimensions($data['dimensions']);
        return $boxType;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cache = $this->app['doctrine.cache'];
        if (!$boxType = $cache->fetch('box_type')) {

            $sql = 'SELECT * FROM box_type';
            $data = $this->app['db']->fetchAll($sql);

            foreach ($data as $info) {
                $boxType[] = $info;
            }

            $cache->save(
                'box_type',
                $boxType,
                $this->app['cache']['lifetime']
            );
        }

        $boxTypeInformation = array();
        foreach ($boxType as $data)
        {
            $boxTypeInformation[] = $this->hydrate($data);
        }

        return $boxTypeInformation;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
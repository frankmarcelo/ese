<?php
/**
 * File: Booking.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Booking
 * This script is the main routing interface to bootstrap
 *
 * Class Booking
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Booking implements \Serializable
{
    private $app;
    protected $id;
    protected $userId;
    protected $boxPricingId;
    protected $boxTypeId;
    protected $pnzCode;
    protected $shipmentDate;
    protected $pickUpDate;
    protected $deliveryName;
    protected $deliveryAddress;
    protected $deliveryEmail;
    protected $deliveryPhone;
    protected $senderName;
    protected $senderAddress;
    protected $senderEmail;
    protected $senderPhone;
    protected $amount;
    protected $estimatedAmount;
    protected $additionalCharges;
    protected $isJumboBox;
    protected $remarks;
    protected $isPickedUp;
    protected $dteCreated;
    protected $dteUpdated;
    protected $bookingItems = array();
    protected $bookingUserName;
    protected $bookingUserEmail;
    protected $bookingUserMobile;
    protected $bookingUserAddress;
    protected $bookingUserGeocodeX;
    protected $bookingUserGeocodeY;
    protected $boxTypeName;
    protected $errors = array();

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $userId
     * @return void
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $boxPricingId
     * @return void
     */
    public function setBoxPricingId($boxPricingId)
    {
        $this->boxPricingId = $boxPricingId;
    }

    /**
     * @return mixed
     */
    public function getBoxPricingId()
    {
        return $this->boxPricingId;
    }

    /**
     * @param $boxTypeId
     */
    public function setBoxTypeId($boxTypeId)
    {
        $this->boxTypeId = $boxTypeId;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeId()
    {
        return $this->boxTypeId;
    }

    /**
     * @param $pnzCode
     * @return void
     */
    public function setPnzCode($pnzCode)
    {
        $this->pnzCode = strtoupper(trim($pnzCode));
    }

    /**
     * @return mixed
     */
    public function getPnzCode()
    {
        return strtoupper($this->pnzCode);
    }

    /**
     * @param $shipmentDate
     */
    public function setShipmentDate($shipmentDate)
    {
        $this->shipmentDate = $shipmentDate;
    }

    /**
     * @return mixed
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }

    /**
     * @param $pickUpDate
     * @return void
     */
    public function setPickUpDate($pickUpDate)
    {
        $this->pickUpDate = $pickUpDate;
    }

    /**
     * @return mixed
     */
    public function getPickUpDate()
    {
        return $this->pickUpDate;
    }

    /**
     * @param $deliveryName
     * @return void
     */
    public function setDeliveryName($deliveryName)
    {
        $this->deliveryName = $deliveryName;
    }

    /**
     * @return mixed
     */
    public function getDeliveryName()
    {
        return $this->deliveryName;
    }

    /**
     * @param $deliveryAddress
     * @return void
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param $deliveryEmail
     * @return void
     */
    public function setDeliveryEmail($deliveryEmail)
    {
        $this->deliveryEmail = $deliveryEmail;
    }

    /**
     * @return mixed
     */
    public function getDeliveryEmail()
    {
        return $this->deliveryEmail;
    }

    /**
     * @param $deliveryPhone
     * @return void
     */
    public function setDeliveryPhone($deliveryPhone)
    {
        $this->deliveryPhone = $deliveryPhone;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * @param $senderName
     * @return void
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * @return mixed
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param $senderAddress
     * @return void
     */
    public function setSenderAddress($senderAddress)
    {
        $this->senderAddress = $senderAddress;
    }

    /**
     * @return mixed
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
     * @param $senderEmail
     * @return void
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param $senderPhone
     * @return void
     */
    public function setSenderPhone($senderPhone)
    {
        $this->senderPhone = $senderPhone;
    }

    /**
     * @return mixed
     */
    public function getSenderPhone()
    {
        return $this->senderPhone;
    }

    /**
     * @param $amount
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $estimatedAmount
     * @return void
     */
    public function setEstimatedAmount($estimatedAmount)
    {
        $this->estimatedAmount = $estimatedAmount;
    }

    /**
     * @return mixed
     */
    public function getEstimatedAmount()
    {
        return $this->estimatedAmount;
    }

    /**
     * @param $additionalCharges
     * @return void
     */
    public function setAdditionalCharges($additionalCharges)
    {
        $this->additionalCharges = $additionalCharges;
    }

    /**
     * @return mixed
     */
    public function getAdditionalCharges()
    {
        return $this->additionalCharges;
    }

    /**
     * @param $isJumboBox
     * @return void
     */
    public function setIsJumboBox($isJumboBox)
    {
        $this->isJumboBox = $isJumboBox;
    }

    /**
     * @return mixed
     */
    public function getIsJumboBox()
    {
        return $this->isJumboBox;
    }

    /**
     * @param $remarks
     * @return void
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param $isPickedUp
     * @return void
     */
    public function setIsPickedUp($isPickedUp)
    {
        $this->isPickedUp = $isPickedUp;
    }

    /**
     * @return mixed
     */
    public function getIsPickedUp()
    {
        return $this->isPickedUp;
    }

    /**
     * @param array $bookingItems
     * @param array $bookingItems
     */
    public function setBookingItems(array $bookingItems)
    {
        $this->bookingItems = $bookingItems;
    }

    /**
     * @return array
     */
    public function getBookingItems()
    {
        return $this->bookingItems;
    }

    /**
     * @param $bookingUserEmail
     * @return void
     */
    public function setBookingUserEmail($bookingUserEmail)
    {
        $this->bookingUserEmail = $bookingUserEmail;
    }

    /**
     * @return mixed
     */
    public function getBookingUserEmail()
    {
        return $this->bookingUserEmail;
    }

    /**
     * @param $bookingUserName
     * @return void
     */
    public function setBookingUserName($bookingUserName)
    {
        $this->bookingUserName = $bookingUserName;
    }

    /**
     * @param $boxTypeName
     */
    public function setBoxTypeName($boxTypeName)
    {
        $this->boxTypeName = $boxTypeName;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeName()
    {
        return $this->boxTypeName;
    }

    /**
     * @return mixed
     */
    public function getBookingUserName()
    {
        return $this->bookingUserName;
    }

    /**
     * @param $bookingUserAddress
     * @return void
     */
    public function setBookingUserAddress($bookingUserAddress)
    {
        $this->bookingUserAddress = $bookingUserAddress;
    }

    /**
     * @return mixed
     */
    public function getBookingUserAddress()
    {
        return $this->bookingUserAddress;
    }

    /**
     * @param $bookingUserGeocodeX
     * @return void
     */
    public function setBookingUserGeocodeX($bookingUserGeocodeX)
    {
        $this->bookingUserGeocodeX = $bookingUserGeocodeX;
    }

    /**
     * @return mixed
     */
    public function getBookingUserGeocodeX()
    {
        return $this->bookingUserGeocodeX;
    }

    /**
     * @param $bookingUserGeocodeY
     * @return void
     */
    public function setBookingUserGeocodeY($bookingUserGeocodeY)
    {
        $this->bookingUserGeocodeY = $bookingUserGeocodeY;
    }

    /**
     * @return mixed
     */
    public function getBookingUserGeocodeY()
    {
        return $this->bookingUserGeocodeY;
    }

    /**
     * @param $bookingUserMobile
     * @return void
     */
    public function setBookingUserMobile($bookingUserMobile)
    {
        $this->bookingUserMobile = $bookingUserMobile;
    }

    /**
     * @return mixed
     */
    public function getBookingUserMobile()
    {
        return $this->bookingUserMobile;
    }

    /**
     * @param $dteCreated
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     *
     */
    public function setDteUpdated()
    {
        $this->dteUpdated = date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteUpdated()
    {
        return $this->dteUpdated;
    }

    /**
     * @param array $data
     * @param bool $extraFields
     * @return Booking
     */
    protected function hydrate(array $data, $extraFields = true)
    {
        $booking = new Booking($this->app);
        $booking->setId($data['id']);
        $booking->setUserId($data['user_id']);
        $booking->setBoxPricingId($data['box_pricing_id']);
        $booking->setBoxTypeId($data['box_type_id']);
        $booking->setPnzCode($data['pnz_code']);
        $booking->setShipmentDate($data['shipment_date']);
        $booking->setPickUpDate($data['pick_up_date']);
        $booking->setDeliveryName($data['delivery_name']);
        $booking->setDeliveryAddress($data['delivery_address']);
        $booking->setDeliveryEmail($data['delivery_email']);
        $booking->setDeliveryPhone($data['delivery_phone']);
        $booking->setSenderName($data['sender_name']);
        $booking->setSenderAddress($data['sender_address']);
        $booking->setSenderEmail($data['sender_email']);
        $booking->setSenderPhone($data['sender_phone']);
        $booking->setAmount($data['amount']);
        $booking->setEstimatedAmount($data['estimated_amount']);
        $booking->setAdditionalCharges($data['additional_charges']);
        $booking->setIsJumboBox($data['is_jumbo_box']);
        $booking->setIsPickedUp($data['is_picked_up']);
        $booking->setRemarks($data['remarks']);
        $booking->setDteCreated($data['dte_created']);
        $booking->setDteUpdated($data['dte_updated']);
        $booking->setBookingUserEmail($data['email']);
        $booking->setBookingUserName($data['name']);
        $booking->setBookingUserAddress($data['address']);
        $booking->setBookingUserMobile($data['mobile']);
        $booking->setBookingUserGeocodeX($data['geocode_x']);
        $booking->setBookingUserGeocodeY($data['geocode_y']);
        $booking->setBoxTypeName($data['description']);

        if ($extraFields == true) {
            $bookingItems = new BookingItems($this->app);
            $booking->setBookingItems($bookingItems->findByBookingId($booking));
        }

        return $booking;
    }

    /**
     * @return array
     */
    public function findAllBookings()
    {
        $sql = 'SELECT  b.*,
                        u.email,
                        u.name,
                        bt.description,
                        u.address,
                        u.geocode_x,
                        u.geocode_y,
                        u.mobile
                        FROM booking b
                            JOIN users u ON (u.id = b.user_id)
                            JOIN box_type bt ON (bt.id = b.box_type_id)
                                ORDER BY dte_created';

        $data = $this->app['db']->fetchAll($sql);
        $boxType = array();
        foreach ($data as $typeData) {
            $boxType[] = $this->hydrate($typeData);
        }

        return $boxType;
    }

    /**
     * @param $isPickedUp
     * @return array
     */
    public function findAll($isPickedUp)
    {
        $sql = 'SELECT  b.*,
                        u.email,
                        u.name,
                        bt.description,
                        u.address,
                        u.geocode_x,
                        u.geocode_y,
                        u.mobile
                        FROM booking b
                            JOIN users u ON (u.id = b.user_id)
                            JOIN box_type bt ON (bt.id = b.box_type_id)
                              WHERE is_picked_up = :is_picked_up
                                  ORDER BY pick_up_date DESC';
        $params = array(
            'is_picked_up' => $isPickedUp
        );
        $data = $this->app['db']->fetchAll($sql, $params);
        $boxType = array();
        foreach ($data as $typeData) {
            $boxType[] = $this->hydrate($typeData);
        }

        return $boxType;
    }

    /**
     * @param $isPickedUp
     * @param $userId
     * @return array
     */
    public function findAllBookingByStatusAndUserId($isPickedUp, $userId)
    {
        $sql = 'SELECT  b.*,
                        u.email,
                        u.name,
                        bt.description,
                        u.address,
                        u.geocode_x,
                        u.geocode_y,
                        u.mobile
                        FROM booking b
                            JOIN users u ON (u.id = b.user_id)
                            JOIN box_type bt ON (bt.id = b.box_type_id)
                              WHERE b.is_picked_up =:is_picked_up AND b.user_id =:user_id
                                  ORDER BY dte_updated';
        $params = array(
            'is_picked_up' => $isPickedUp,
            'user_id' => $userId
        );
        $data = $this->app['db']->fetchAll($sql, $params);
        $boxType = array();
        foreach ($data as $typeData) {
            $boxType[] = $this->hydrate($typeData);
        }

        return $boxType;
    }

    /**
     * @param $userId
     * @return array
     */
    public function findAllByUserId($userId)
    {
        $sql = 'SELECT  b.*,
                        u.email,
                        u.name,
                        bt.description,
                        u.address,
                        u.geocode_x,
                        u.geocode_y,
                        u.mobile
                        FROM booking b
                            JOIN users u ON (u.id = b.user_id)
                            JOIN box_type bt ON (bt.id = b.box_type_id)
                              WHERE b.user_id =:user_id
                                  ORDER BY pick_up_date DESC';
        $params = array(
            'user_id' => $userId
        );
        $data = $this->app['db']->fetchAll($sql, $params);
        $boxType = array();
        foreach ($data as $typeData) {
            $boxType[] = $this->hydrate($typeData);
        }

        return $boxType;
    }

    /**
     * @param bool $monthlyBookings
     * @return mixed
     */
    public function countAllBookings($monthlyBookings = false)
    {
        $sql = 'SELECT count(*) as total FROM bookings ';
        if ($monthlyBookings) {
            $sql .= "WHERE DATE_FORMAT(dte_created, '%Y-%m')";
        }
        $data = $this->app['db']->fetchAll($sql);
        return $data;
    }

    /**
     * @return array
     */
    public function findAllLatestBookings()
    {
        $sql = "
        SELECT  b.*,
                u.email,
                u.name,
                bt.description,
                u.address,
                u.geocode_x,
                u.geocode_y,
                u.mobile
            FROM booking b
                JOIN users u ON (u.id = b.user_id)
                JOIN box_type bt ON (bt.id = b.box_type_id)
                    WHERE
                        b.is_picked_up  = :is_picked_up AND
                        b.pick_up_date >= :pick_up_date
                        ORDER BY dte_created DESC
                          LIMIT 5";

        $params = array(
            'is_picked_up' => 0,
            'pick_up_date' => date("Y-m-d")
        );

        $data = $this->app['db']->fetchAll($sql, $params);

        $bookingsLatestInformation = array();
        foreach ($data as $bookingInfo)
        {
            $bookingsLatestInformation[] = $this->hydrate($bookingInfo, $extraFields = false);
        }

        return $bookingsLatestInformation;
    }

    /**
     * @param int $pickedUpPending
     * @param int $pickUpConfirmed
     * @return array
     */
    public function findAllBookingByMonth($pickedUpPending = 0, $pickUpConfirmed = 1)
    {
        $sql = "
        SELECT  b.*,
                u.email,
                u.name,
                bt.description,
                u.address,
                u.geocode_x,
                u.geocode_y,
                u.mobile
            FROM booking b
                JOIN users u ON (u.id = b.user_id)
                JOIN box_type bt ON (bt.id = b.box_type_id)
                    WHERE
                        DATE_FORMAT(b.pick_up_date, '%Y-%m') =:dte_created AND
                        (b.is_picked_up =:pick_up_pending OR b.is_picked_up =:pick_up_confirmed)
                        ORDER BY b.pick_up_date ";

        $params = array(
            'dte_created' => date("Y-m"),
            'pick_up_pending' => $pickedUpPending,
            'pick_up_confirmed' => $pickUpConfirmed
        );

        $data = $this->app['db']->fetchAll($sql, $params);

        $bookingsLatestInformation = array();
        foreach ($data as $bookingInfo)
        {
            $bookingsLatestInformation[] = $this->hydrate($bookingInfo, $extraFields = false);
        }

        return $bookingsLatestInformation;
    }

    /**
     * @param $pnzCode
     * @return Booking
     */
    public function findByPnzCode($pnzCode)
    {

        $sql = "
            SELECT  b.*,
                    u.email,
                    u.name,
                    bt.description,
                    u.address,
                    u.geocode_x,
                    u.geocode_y,
                    u.mobile
                FROM booking b
                    JOIN users u ON (u.id = b.user_id)
                    JOIN box_type bt ON (bt.id = b.box_type_id)
                        WHERE
                            b.pnz_code = :pnz_code";

        $params = array(
            'pnz_code' => strtoupper($pnzCode)
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        if ($data) {
            return $this->hydrate($data[0], $extraFields = true);
        } else {
            return false;
        }
    }

    /**
     * @param Booking $booking
     * @return mixed
     */
    public function save(Booking $booking)
    {
        $sql = 'INSERT INTO booking
            (
              user_id,
              box_pricing_id,
              box_type_id,
              pnz_code,
              shipment_date,
              pick_up_date,
              delivery_name,
              delivery_address,
              delivery_email,
              delivery_phone,
              sender_name,
              sender_address,
              sender_email,
              sender_phone,
              estimated_amount,
              amount,
              additional_charges,
              is_jumbo_box,
              is_picked_up,
              remarks,
              dte_created
            )
            VALUES
            (
              :user_id,
              :box_pricing_id,
              :box_type_id,
              :pnz_code,
              :shipment_date,
              :pick_up_date,
              :delivery_name,
              :delivery_address,
              :delivery_email,
              :delivery_phone,
              :sender_name,
              :sender_address,
              :sender_email,
              :sender_phone,
              :estimated_amount,
              :amount,
              :additional_charges,
              :is_jumbo_box,
              :is_picked_up,
              :remarks,
              :dte_created
            ) ';

        $params = array(
            'user_id' => $booking->getUserId(),
            'box_pricing_id' => $booking->getBoxPricingId(),
            'box_type_id' => $booking->getBoxTypeId(),
            'pnz_code' => $booking->getPnzCode(),
            'shipment_date' => $booking->getShipmentDate(),
            'pick_up_date' => $booking->getPickUpDate(),
            'delivery_name' => $booking->getDeliveryName(),
            'delivery_address' => $booking->getDeliveryAddress(),
            'delivery_email' => $booking->getDeliveryEmail(),
            'delivery_phone' => $booking->getDeliveryPhone(),
            'sender_name' => $booking->getSenderName(),
            'sender_address' => $booking->getSenderAddress(),
            'sender_email' => $booking->getSenderEmail(),
            'sender_phone' => $booking->getSenderPhone(),
            'estimated_amount' => $booking->getEstimatedAmount(),
            'amount' => $booking->getAmount(),
            'additional_charges' => $booking->getAdditionalCharges(),
            'is_jumbo_box' => $booking->getIsJumboBox(),
            'is_picked_up' => $booking->getIsPickedUp(),
            'remarks' => $booking->getRemarks(),
            'dte_created' => $booking->getDteCreated()
        );

        $this->app['db']->executeUpdate($sql, $params);

        return $this->app['db']->lastInsertId();
    }

    /**
     * @param Booking $booking
     * @param $id
     * @return mixed
     */
    public function update(Booking $booking, $id)
    {
        $sql = '
            UPDATE booking SET
              box_pricing_id =:box_pricing_id,
              box_type_id =:box_type_id,
              pnz_code =:pnz_code,
              shipment_date =:shipment_date,
              pick_up_date =:pick_up_date,
              delivery_name =:delivery_name,
              delivery_address =:delivery_address,
              delivery_email =:delivery_email,
              delivery_phone =:delivery_phone,
              sender_name =:sender_name,
              sender_address =:sender_address,
              sender_email =:sender_email,
              sender_phone =:sender_phone,
              estimated_amount =:estimated_amount,
              amount =:amount,
              additional_charges =:additional_charges,
              is_jumbo_box =:is_jumbo_box,
              is_picked_up =:is_picked_up,
              remarks =:remarks,
              dte_updated =:dte_updated
            WHERE id =:id
            ';

        $params = array(
            'id' => $id,
            'box_pricing_id' => $booking->getBoxPricingId(),
            'box_type_id' => $booking->getBoxTypeId(),
            'pnz_code' => $booking->getPnzCode(),
            'shipment_date' => $booking->getShipmentDate(),
            'pick_up_date' => $booking->getPickUpDate(),
            'delivery_name' => $booking->getDeliveryName(),
            'delivery_address' => $booking->getDeliveryAddress(),
            'delivery_email' => $booking->getDeliveryEmail(),
            'delivery_phone' => $booking->getDeliveryPhone(),
            'sender_name' => $booking->getSenderName(),
            'sender_address' => $booking->getSenderAddress(),
            'sender_email' => $booking->getSenderEmail(),
            'sender_phone' => $booking->getSenderPhone(),
            'estimated_amount' => $booking->getEstimatedAmount(),
            'amount' => $booking->getAmount(),
            'additional_charges' => $booking->getAdditionalCharges(),
            'is_jumbo_box' => $booking->getIsJumboBox(),
            'is_picked_up' => $booking->getIsPickedUp(),
            'remarks' => $booking->getRemarks(),
            'dte_updated' => $booking->getDteUpdated()
        );

        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param $pnzCode
     * @return bool
     */
    public function deleteBookingByPnzCode($pnzCode)
    {
        $sql = '
            DELETE FROM booking
                WHERE pnz_code = :pnz_code';

        $params = array(
            'pnz_code' => $pnzCode
        );

        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param int $isPickedUp
     * @return bool
     */
    public function updateBookingStatusById($isPickedUp = 1)
    {
        $sql = "
            UPDATE booking
                SET is_picked_up = :is_picked_up
                  WHERE id = :id
                ";

        $params = array(
            'is_picked_up' => $isPickedUp,
            'id' => $this->getId()
        );

        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param Request $request
     * @param bool $isEdit
     * @param null $id
     * @return bool
     */
    public function validate(Request $request, $isEdit = false, $id = null)
    {

        if ($request->request->get('boxTypeId') < 1) {
            $this->errors['boxTypeId'] = 'Box type cannot be empty.';
        }

        if ($request->request->get('boxLocationId') < 1) {
            $this->errors['boxTypeId'] = 'Destination of box is needed to determine the delivery amount.';
        }

        if (strlen(trim($request->request->get('pickUpDate'))) < 1) {
            $this->errors['pickUpDate'] = 'Pick up date cannot be empty.';
        }

        if ($isEdit == false) {
            $pnzCode = $this->findByPnzCode($request->request->get('pnzCode'));

            if ($pnzCode && $pnzCode->getId() > 0) {
                $this->errors['pnzCode'] = 'PNZ Code has to be unique or has not been used.';
            }

            if (strlen(trim($request->request->get('pnzCode'))) < 1) {
                $this->errors['pnzCode'] = 'Invalid Pnz code.';
            }

            if (strpos(strtoupper($request->request->get('pnzCode')),'PNZ') != 0) {
                $this->errors['pnzCode'] = 'PNZ Code should start with PNZ.';
            }
        } else {

            $pnzCode = $this->findByPnzCode($request->request->get('pnzCode'));

            if ($pnzCode && $pnzCode->getId() > 0 && $pnzCode->getUserId() !== $id) {
                $this->errors['pnzCode'] = 'PNZ Code has to be unique or has not been used.';
            }

            if (strlen(trim($request->request->get('pnzCode'))) < 1) {
                $this->errors['pnzCode'] = 'Invalid Pnz code.';
            }

            if (strpos(strtoupper($request->request->get('pnzCode')),'PNZ') != 0) {
                $this->errors['pnzCode'] = 'PNZ Code should start with PNZ.';
            }
        }

        if (strlen(trim($request->request->get('deliveryName'))) < 1) {
            $this->errors['deliveryName'] = 'Delivery name cannot be empty.';
        }

        if (strlen(trim($request->request->get('deliveryAddress'))) < 1) {
            $this->errors['deliveryAddress'] = 'Delivery address cannot be empty.';
        }

        if (strlen(trim($request->request->get('deliveryEmail'))) < 1) {
            $this->errors['deliveryEmail'] = 'Delivery email cannot be empty.';
        }

        if (strlen(trim($request->request->get('deliveryPhone'))) < 1) {
            $this->errors['deliveryPhone'] = 'Delivery phone cannot be empty.';
        }

        if (strlen(trim($request->request->get('senderName'))) < 1) {
            $this->errors['senderName'] = 'Sender name cannot be empty.';
        }

        if (strlen(trim($request->request->get('senderAddress'))) < 1) {
            $this->errors['senderAddress'] = 'Sender address cannot be empty.';
        }

        if (strlen(trim($request->request->get('senderEmail'))) < 1) {
            $this->errors['senderEmail'] = 'Sender email cannot be empty.';
        }

        if (strlen(trim($request->request->get('senderEmail'))) < 1) {
            $this->errors['senderPhone'] = 'Sender phone cannot be empty.';
        }

        if ($request->request->get('name')) {
            $names = $request->request->get('name');

            $categories = $request->request->get('categoryId');
            $prices = $request->request->get('price');
            $quantities = $request->request->get('quantity');
            $totalBookingItems = 0;
            foreach ($names as $key => $name) {
                if ($key > 0) {
                    if (strlen(trim($names[$key])) < 1
                    ||  strlen(trim($categories[$key])) < 1
                    ||  strlen(trim($prices[$key])) < 1
                    ||  strlen(trim($quantities[$key]) < 1)
                    ) {

                        continue;
                    } else {

                        $totalBookingItems++;

                    }
                }
            }

            if ($totalBookingItems < 1) {
                $this->errors['items'] = 'Item information cannot be empty.';
            }
        }

        return empty($this->errors) ? true: false;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: BoxPricing.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class BoxPricing
 * This script is the main routing interface to bootstrap
 *
 * Class BoxPricing
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class BoxPricing implements \Serializable
{
    private $app;
    protected $id;
    protected $boxTypeId;
    protected $boxLocationId;
    protected $amount;
    protected $boxLocationName;
    protected $boxTypeDescription;
    protected $boxTypeDimensions;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $boxTypeId
     */
    public function setBoxTypeId($boxTypeId)
    {
        $this->boxTypeId = $boxTypeId;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeId()
    {
        return $this->boxTypeId;
    }

    /**
     * @param $boxLocationId
     */
    public function setBoxLocationId($boxLocationId)
    {
        $this->boxLocationId = $boxLocationId;
    }

    /**
     * @return mixed
     */
    public function getBoxLocationId()
    {
        return $this->boxLocationId;
    }

    /**
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $boxLocationName
     */
    public function setBoxLocationName($boxLocationName)
    {
        $this->boxLocationName = $boxLocationName;
    }

    /**
     * @return mixed
     */
    public function getBoxLocationName()
    {
        return $this->boxLocationName;
    }

    /**
     * @param $boxTypeDescription
     */
    public function setBoxTypeDescription($boxTypeDescription)
    {
        $this->boxTypeDescription = $boxTypeDescription;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeDescription()
    {
        return $this->boxTypeDescription;
    }

    /**
     * @param $boxTypeDimensions
     */
    public function setBoxTypeDimensions($boxTypeDimensions)
    {
        $this->boxTypeDimensions = $boxTypeDimensions;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeDimensions()
    {
        return $this->boxTypeDimensions;
    }

    /**
     * @param array $data
     * @return BoxPricing
     */
    protected function hydrate(array $data)
    {
        $boxPricing = new BoxPricing($this->app);
        $boxPricing->setId($data['id']);
        $boxPricing->setBoxLocationId($data['box_location_id']);
        $boxPricing->setBoxTypeId($data['box_type_id']);
        $boxPricing->setAmount($data['amount']);
        $boxPricing->setBoxLocationName($data['name']);
        $boxPricing->setBoxTypeDescription($data['description']);
        $boxPricing->setBoxTypeDimensions($data['dimensions']);
        return $boxPricing;
    }

    /**
     * @param null $boxTypeId
     * @param null $boxLocationId
     * @return BoxPricing
     */
    public function findByBoxTypeIdAndBoxLocationId($boxTypeId = null, $boxLocationId = null)
    {
        $sql = '
            SELECT  bp.*,
                    bl.name,
                    bt.description,
                    bt.dimensions
                FROM box_pricing bp
                    JOIN box_location bl ON (bl.id=bp.box_location_id)
                    JOIN box_type bt ON (bt.id=bp.box_type_id)
                        WHERE bp.is_display = 1 AND
                              bp.box_type_id =:box_type_id AND
                              bp.box_location_id =:box_location_id';

        $params = array(
            'box_type_id' => $boxTypeId,
            'box_location_id' => $boxLocationId
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        return $this->hydrate($data[0]);
    }

    /**
     * @param $boxPricingId
     * @return BoxPricing
     */
    public function findById($boxPricingId)
    {
        $sql = '
            SELECT  bp.*,
                    bl.name,
                    bt.description,
                    bt.dimensions
                FROM box_pricing bp
                    JOIN box_location bl ON (bl.id=bp.box_location_id)
                    JOIN box_type bt ON (bt.id=bp.box_type_id)
                        WHERE bp.id =:box_pricing_id';

        $params = array(
            'box_pricing_id' => $boxPricingId
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        return $this->hydrate($data[0]);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cache = $this->app['doctrine.cache'];
        if (!$pricing = $cache->fetch('pricing')) {

            $sql = '
            SELECT  bp.*,
                    bl.name,
                    bt.description,
                    bt.dimensions
                FROM box_pricing bp
                    JOIN box_location bl ON (bl.id=bp.box_location_id)
                    JOIN box_type bt ON (bt.id=bp.box_type_id)
                        WHERE bp.is_display = 1
                           ORDER BY bp.box_location_id, bp.amount DESC';
            $data = $this->app['db']->fetchAll($sql);

            foreach ($data as $pricingInfo) {
                $pricing[] = $pricingInfo;
            }

            $cache->save(
                'pricing',
                $pricing,
                $this->app['cache']['lifetime']
            );
        }

        $pricingInformation = array();
        foreach ($pricing as $pricingData)
        {
            $pricingInformation[] = $this->hydrate($pricingData);
        }

        return $pricingInformation;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: Zone.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class Zone
 * This script is the main routing interface to bootstrap
 *
 * Class Zone
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Zone implements \Serializable
{
    private $app;
    protected $id;
    protected $zoneScheduleId;
    protected $name;
    protected $isActive;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $zoneScheduleId
     */
    public function setZoneScheduleId($zoneScheduleId)
    {
        $this->zoneScheduleId = $zoneScheduleId;
    }

    /**
     * @return mixed
     */
    public function getZoneScheduleId()
    {
        return $this->zoneScheduleId;
    }

    /**
     * @param $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }

    /**
     * @param array $data
     * @return Location
     */
    protected function hydrate(array $data)
    {
        $zone = new Zone($this->app);
        $zone->setId($data['id']);
        $zone->setZoneScheduleId($data['zone_schedule_id']);
        $zone->setIsActive($data['is_active']);
        $zone->setName($data['name']);
        $zone->setDteCreated($data['dte_created']);

        $zoneSchedule = new ZoneSchedule($this->app);
        $zoneSchedule->setId($data['zone_schedule_id']);
        $zoneSchedule->setName($data['scheduleName']);
        $zoneSchedule->setIsMonday($data['is_monday']);
        $zoneSchedule->setIsTuesday($data['is_tuesday']);
        $zoneSchedule->setIsWednesday($data['is_wednesday']);
        $zoneSchedule->setIsThursday($data['is_thursday']);
        $zoneSchedule->setIsFriday($data['is_friday']);
        $zoneSchedule->setIsSaturday($data['is_saturday']);
        $zoneSchedule->setIsSunday($data['is_sunday']);

        return array(
            'zone' => $zone,
            'zoneSchedule' => $zoneSchedule
        );
    }

    /**
     * @param $zoneId
     * @return bool|\Ese\Models\Location
     * @return bool
     */
    public function findByZoneId($zoneId)
    {
        $sql = '
            SELECT  z.*,
                    zs.name as scheduleName,
                    zs.is_monday,
                    zs.is_tuesday,
                    zs.is_wednesday,
                    zs.is_thursday,
                    zs.is_friday,
                    zs.is_saturday,
                    zs.is_sunday
              FROM zone z
                 JOIN zone_schedule zs ON (zs.id = z.zone_schedule_id)
                  WHERE z.id =:id';

        $params = array(
            'id' => $zoneId,
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        if ($data) {
            return $this->hydrate($data[0]);
        }

        return false;
    }
}
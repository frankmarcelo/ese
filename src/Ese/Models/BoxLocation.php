<?php
/**
 * File: BoxLocation.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class BoxLocation
 * This script is the main routing interface to bootstrap
 *
 * Class BoxLocation
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class BoxLocation implements \Serializable
{
    private $app;
    protected $id;
    protected $name;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param array $data
     * @return BoxLocation
     */
    protected function hydrate(array $data)
    {
        $boxLocation = new BoxLocation($this->app);
        $boxLocation->setId($data['id']);
        $boxLocation->setName($data['name']);
        return $boxLocation;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cache = $this->app['doctrine.cache'];
        if (!$boxLocation = $cache->fetch('box_location')) {

            $sql = 'SELECT * FROM box_location';
            $data = $this->app['db']->fetchAll($sql);

            foreach ($data as $info) {
                $boxLocation[] = $info;
            }

            $cache->save(
                'box_location',
                $boxLocation,
                $this->app['cache']['lifetime']
            );
        }

        $boxLocationInformation = array();
        foreach ($boxLocation as $data)
        {
            $boxLocationInformation[] = $this->hydrate($data);
        }

        return $boxLocationInformation;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: ShipmentSchedule.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class ShipmentSchedule
 * This script is the main routing interface to bootstrap
 *
 * Class ShipmentSchedule
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class ShipmentSchedule implements \Serializable
{
    private $app;
    protected $id;
    protected $name;
    protected $cutOffDate;
    protected $startDate;
    protected $endDate;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $cutOffDate
     */
    public function setCutOffDate($cutOffDate)
    {
        $this->cutOffDate = $cutOffDate;
    }

    /**
     * @return mixed
     */
    public function getCutOffDate()
    {
        return $this->cutOffDate;
    }

    /**
     * @param $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * Reconstitute a User object from stored data.
     *
     * @param array $data
     * @return User
     * @throws \RuntimeException if database schema is out of date.
     */
    protected function hydrate(array $data)
    {
        $shipmentSchedule = new ShipmentSchedule($this->app);
        $shipmentSchedule->setId($data['id']);
        $shipmentSchedule->setName($data['name']);
        $shipmentSchedule->setCutOffDate($data['cut_off_date']);
        $shipmentSchedule->setStartDate($data['start_date']);
        $shipmentSchedule->setEndDate($data['end_date']);
        $shipmentSchedule->setDteCreated($data['dte_created']);
        return $shipmentSchedule;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cache = $this->app['doctrine.cache'];
        if (!$schedules = $cache->fetch('shipment')) {

            $sql = 'SELECT * FROM shipment_schedule';
            $data = $this->app['db']->fetchAll($sql);

            foreach ($data as $shipmentData) {
                $schedules[] = $shipmentData;
            }

            $cache->save(
                'shipment',
                $schedules,
                $this->app['cache']['lifetime']
            );
        }

        $scheduleInformation = array();
        foreach ($schedules as $schedule)
        {
            $scheduleInformation[] = $this->hydrate($schedule);
        }

        return $scheduleInformation;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: RequestBox.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class RequestBox
 * This script is the main routing interface to bootstrap
 *
 * Class RequestBox
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class RequestBox implements \Serializable
{
    private $app;
    protected $id;
    protected $userId;
    protected $boxTypeId;
    protected $requestDate;
    protected $isDelivered;
    protected $dteCreated;
    protected $dteUpdated;
    protected $bookingUserName;
    protected $bookingUserEmail;
    protected $bookingUserMobile;
    protected $bookingUserAddress;
    protected $bookingUserGeocodeX;
    protected $bookingUserGeocodeY;
    protected $boxTypeName;
    protected $errors = array();

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $userId
     * @return void
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $boxTypeId
     */
    public function setBoxTypeId($boxTypeId)
    {
        $this->boxTypeId = $boxTypeId;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeId()
    {
        return $this->boxTypeId;
    }

    /**
     * @param $requestDate
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
    }

    /**
     * @return mixed
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * @param $isDelivered
     */
    public function setIsDelivered($isDelivered)
    {
        $this->isDelivered = $isDelivered;
    }

    /**
     * @return mixed
     */
    public function getIsDelivered()
    {
        return $this->isDelivered;
    }

    /**
     * @param array $data
     * @return RequestBox
     */
    protected function hydrate(array $data)
    {
        $requestBox = new RequestBox($this->app);
        $requestBox->setId($data['id']);
        $requestBox->setUserId($data['user_id']);
        $requestBox->setBoxTypeId($data['box_type_id']);
        $requestBox->setRequestDate($data['request_date']);
        $requestBox->setIsDelivered($data['is_delivered']);
        $requestBox->setDteCreated($data['dte_created']);
        $requestBox->setDteUpdated($data['dte_updated']);
        $requestBox->setBookingUserName($data['name']);
        $requestBox->setBookingUserAddress($data['address']);
        $requestBox->setBookingUserMobile($data['mobile']);
        $requestBox->setBookingUserGeocodeX($data['geocode_x']);
        $requestBox->setBookingUserGeocodeY($data['geocode_y']);
        $requestBox->setBoxTypeName($data['description']);
        return $requestBox;
    }

    /**
     * @param $bookingUserName
     * @return void
     */
    public function setBookingUserName($bookingUserName)
    {
        $this->bookingUserName = $bookingUserName;
    }

    /**
     * @param $boxTypeName
     */
    public function setBoxTypeName($boxTypeName)
    {
        $this->boxTypeName = $boxTypeName;
    }

    /**
     * @return mixed
     */
    public function getBoxTypeName()
    {
        return $this->boxTypeName;
    }

    /**
     * @return mixed
     */
    public function getBookingUserName()
    {
        return $this->bookingUserName;
    }

    /**
     * @param $bookingUserAddress
     * @return void
     */
    public function setBookingUserAddress($bookingUserAddress)
    {
        $this->bookingUserAddress = $bookingUserAddress;
    }

    /**
     * @return mixed
     */
    public function getBookingUserAddress()
    {
        return $this->bookingUserAddress;
    }

    /**
     * @param $bookingUserGeocodeX
     * @return void
     */
    public function setBookingUserGeocodeX($bookingUserGeocodeX)
    {
        $this->bookingUserGeocodeX = $bookingUserGeocodeX;
    }

    /**
     * @return mixed
     */
    public function getBookingUserGeocodeX()
    {
        return $this->bookingUserGeocodeX;
    }

    /**
     * @param $bookingUserGeocodeY
     * @return void
     */
    public function setBookingUserGeocodeY($bookingUserGeocodeY)
    {
        $this->bookingUserGeocodeY = $bookingUserGeocodeY;
    }

    /**
     * @return mixed
     */
    public function getBookingUserGeocodeY()
    {
        return $this->bookingUserGeocodeY;
    }

    /**
     * @param $bookingUserMobile
     * @return void
     */
    public function setBookingUserMobile($bookingUserMobile)
    {
        $this->bookingUserMobile = $bookingUserMobile;
    }

    /**
     * @return mixed
     */
    public function getBookingUserMobile()
    {
        return $this->bookingUserMobile;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $sql = "
        SELECT  rq.*,
                u.email,
                u.name,
                bt.description,
                u.address,
                u.geocode_x,
                u.geocode_y,
                u.mobile
            FROM request_box rq
              JOIN users u ON (u.id = rq.user_id)
              JOIN box_type bt ON (bt.id = rq.box_type_id)
                  WHERE rq.is_delivered = 0
                  ORDER BY rq.request_date ASC";

        $data = $this->app['db']->fetchAll($sql);

        $requestBoxes = array();
        foreach ($data as $dataBox)
        {
            $requestBoxes[] = $this->hydrate($dataBox);
        }
        return $requestBoxes;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if ($this->getUserId() < 1) {
            $this->errors['userId'] = 'User session is invalid.';
        }

        if ($this->getRequestDate() == '') {
            $this->errors['requestDate'] = 'Request date is invalid.';
        }

        if ($this->getBoxTypeId() == '') {
            $this->errors['requestDate'] = 'Box type is invalid.';
        }

        return (sizeof($this->errors) < 1) ? true: false;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param RequestBox $requestBox
     * @return mixed
     */
    public function save(RequestBox $requestBox)
    {
        $sql = 'INSERT INTO request_box
            (
              user_id,
              box_type_id,
              request_date,
              is_delivered,
              dte_created
            )
            VALUES
            (
              :user_id,
              :box_type_id,
              :request_date,
              :is_delivered,
              :dte_created
            ) ';

        $params = array(
            'user_id' => $requestBox->getUserId(),
            'box_type_id' => $requestBox->getBoxTypeId(),
            'request_date' => $requestBox->getRequestDate(),
            'is_delivered' => $requestBox->getIsDelivered(),
            'dte_created' => $requestBox->getDteCreated()
        );

        $this->app['db']->executeUpdate($sql, $params);
        return $this->app['db']->lastInsertId();
    }

    /**
     * @param RequestBox $requestBox
     * @return mixed
     */
    public function update(RequestBox $requestBox)
    {
        $sql = '
            UPDATE request_box
              SET is_delivered = 1
              WHERE id = :id';

        $params = array(
            'id' => $requestBox->getId(),
        );

        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * Set the time the user was originally created.
     *
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    public function setDteUpdated($dteUpdated)
    {
        $this->dteUpdated = $dteUpdated;
    }

    public function getDteUpdated()
    {
        return $this->dteUpdated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
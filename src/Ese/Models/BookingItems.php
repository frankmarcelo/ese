<?php
/**
 * File: BookingItems.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;
use Ese\Models\Booking;

/**
 * Class BookingItems
 * This script is the main routing interface to bootstrap
 *
 * Class BookingItems
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class BookingItems implements \Serializable
{
    private $app;
    protected $id;
    protected $bookingId;
    protected $categoryId;
    protected $name;
    protected $quantity;
    protected $amount;
    protected $dteCreated;
    protected $categoryName;
    protected $identityMap = array();

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $bookingId
     */
    public function setBookingId($bookingId)
    {
        $this->bookingId = $bookingId;
    }

    /**
     * @return mixed
     */
    public function getBookingId()
    {
        return $this->bookingId;
    }

    /**
     * @param $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $categoryName
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * @param $dteCreated
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * @param array $data
     * @return Category
     */
    protected function hydrate(array $data)
    {
        $bookingItems = new BookingItems($this->app);
        $bookingItems->setId($data['id']);
        $bookingItems->setBookingId($data['booking_id']);
        $bookingItems->setCategoryId($data['category_id']);
        $bookingItems->setName($data['name']);
        $bookingItems->setQuantity($data['quantity']);
        $bookingItems->setAmount($data['amount']);
        $bookingItems->setDteCreated($data['dte_created']);
        $bookingItems->setCategoryName($data['category']);
        return $bookingItems;
    }

    /**
     * @param Booking $booking
     * @return array
     */
    public function findByBookingId(Booking $booking)
    {
        $sql = '
            SELECT bi.*, c.name as category
              FROM booking_items bi
                JOIN category c ON (c.id = bi.category_id)
                  WHERE booking_id = :booking_id
                    ORDER BY bi.name';

        $params = array(
            'booking_id' => $booking->getId(),
        );

        $data = $this->app['db']->fetchAll($sql, $params);

        $bookingItems = array();

        foreach ($data as $bookingItem) {
            $bookingItems[] = $this->hydrate($bookingItem);
        }

        return $bookingItems;
    }

    /**
     * @param BookingItems $bookingItem
     * @internal param \Ese\Models\Booking $booking
     * @return mixed
     */
    public function save(BookingItems $bookingItem)
    {
        $sql = 'INSERT INTO booking_items
            (
              booking_id,
              category_id,
              name,
              quantity,
              amount,
              dte_created
            )
            VALUES
            (
              :booking_id,
              :category_id,
              :name,
              :quantity,
              :amount,
              :dte_created
            ) ';

        $params = array(
            'booking_id' => $bookingItem->getBookingId(),
            'category_id' => $bookingItem->getCategoryId(),
            'name' => $bookingItem->getName(),
            'quantity' => $bookingItem->getQuantity(),
            'amount' => $bookingItem->getAmount(),
            'dte_created' => $bookingItem->getDteCreated()
        );

        $this->app['db']->executeUpdate($sql, $params);
        return $this->app['db']->lastInsertId();
    }

    /**
     * @param $bookingId
     */
    public function deleteBookingItemByBookingId($bookingId)
    {
        $sql = '
            DELETE FROM booking_items
                WHERE booking_id = :booking_id';

        $params = array(
            'booking_id' => $bookingId
        );

        $this->app['db']->executeUpdate($sql, $params);
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
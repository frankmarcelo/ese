<?php
/**
 * File: Category.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class Category
 * This script is the main routing interface to bootstrap
 *
 * Class Category
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Category implements \Serializable
{
    private $app;
    protected $id;
    protected $name;
    protected $identityMap = array();
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = $dteCreated;
    }

    /**
     * @param $dteCreated
     */
    public function getDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * @param array $data
     * @return Category
     */
    protected function hydrate(array $data)
    {
        $category = new Category($this->app);
        $category->setId($data['id']);
        $category->setName($data['name']);
        $category->setDteCreated($data['dte_created']);
        return $category;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cache = $this->app['doctrine.cache'];
        if (!$categories = $cache->fetch('categories')) {

            $sql = 'SELECT * FROM category ORDER BY name ASC';
            $data = $this->app['db']->fetchAll($sql);

            foreach ($data as $info) {
                $categories[] = $info;
            }

            $cache->save(
                'categories',
                $categories,
                $this->app['cache']['lifetime']
            );
        }

        $categoryInformation = array();
        foreach ($categories as $data)
        {
            $categoryInformation[] = $this->hydrate($data);
        }

        return $categoryInformation;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
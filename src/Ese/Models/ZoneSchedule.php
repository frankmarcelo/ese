<?php
/**
 * File: ZoneSchedule.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class ZoneSchedule
 * This script is the main routing interface to bootstrap
 *
 * Class ZoneSchedule
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class ZoneSchedule implements \Serializable
{
    private $app;
    protected $id;
    protected $name;
    protected $isMonday;
    protected $isTuesday;
    protected $isWednesday;
    protected $isThursday;
    protected $isFriday;
    protected $isSaturday;
    protected $isSunday;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $isMonday
     */
    public function setIsMonday($isMonday)
    {
        $this->isMonday = $isMonday;
    }

    /**
     * @return mixed
     */
    public function getIsMonday()
    {
        return $this->isMonday;
    }

    /**
     * @param $isTuesday
     */
    public function setIsTuesday($isTuesday)
    {
        $this->isTuesday = $isTuesday;
    }

    /**
     * @return mixed
     */
    public function getIsTuesday()
    {
        return $this->isTuesday;
    }

    /**
     * @param $isWednesday
     */
    public function setIsWednesday($isWednesday)
    {
        $this->isWednesday = $isWednesday;
    }

    /**
     * @return mixed
     */
    public function getIsWednesday()
    {
        return $this->isWednesday;
    }

    /**
     * @param $isThursday
     */
    public function setIsThursday($isThursday)
    {
        $this->isThursday = $isThursday;
    }

    /**
     * @return mixed
     */
    public function getIsThursday()
    {
        return $this->isThursday;
    }

    /**
     * @param $isFriday
     */
    public function setIsFriday($isFriday)
    {
        $this->isFriday = $isFriday;
    }

    /**
     * @return mixed
     */
    public function getIsFriday()
    {
        return $this->isFriday;
    }

    /**
     * @param $isSaturday
     */
    public function setIsSaturday($isSaturday)
    {
        $this->isSaturday = $isSaturday;
    }

    /**
     * @return mixed
     */
    public function getIsSaturday()
    {
        return $this->isSaturday;
    }

    /**
     * @param $isSunday
     */
    public function setIsSunday($isSunday)
    {
        $this->isSunday = $isSunday;
    }

    /**
     * @return mixed
     */
    public function getIsSunday()
    {
        return $this->isSunday;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
<?php
/**
 * File: MonthlyGeneralInfo.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class MonthlyGeneralInfo
 * This script is the main routing interface to bootstrap
 *
 * Class MonthlyGeneralInfo
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class MonthlyGeneralInfo implements \Serializable
{
    private $app;
    protected $id;
    protected $infoYearMonth;
    protected $overallBooking;
    protected $overallAmount;
    protected $dteCreated;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $infoYearMonth
     */
    public function setInfoYearMonth($infoYearMonth)
    {
        $this->infoYearMonth = $infoYearMonth;
    }

    /**
     * @return mixed
     */
    public function getInfoYearMonth()
    {
        return $this->infoYearMonth;
    }

    /**
     * @param $overallBooking
     */
    public function setOverallBooking($overallBooking)
    {
        $this->overallBooking = $overallBooking;
    }

    /**
     * @return mixed
     */
    public function getOverallBooking()
    {
        return $this->overallBooking;
    }

    /**
     * @param $overallAmount
     */
    public function setOverallAmount($overallAmount)
    {
        $this->overallAmount = $overallAmount;
    }

    /**
     * @return mixed
     */
    public function getOverallAmount()
    {
        return $this->overallAmount;
    }

    /**
     * Set the time the user was originally created.
     *
     * @param int $dteCreated A timestamp value.
     */
    public function setDteCreated($dteCreated)
    {
        $this->dteCreated = ($dteCreated) ? $dteCreated : date("Y-m-d H:i:s");
    }

    /**
     * Set the time the user was originally created.
     *
     * @return int
     */
    public function getDteCreated()
    {
        return $this->dteCreated;
    }

    /**
     * @param array $data
     * @return Category
     */
    protected function hydrate(array $data)
    {
        $monthlyGeneralInfo = new MonthlyGeneralInfo($this->app);
        $monthlyGeneralInfo->setId($data['id']);
        $monthlyGeneralInfo->setInfoYearMonth($data['info_year_month']);
        $monthlyGeneralInfo->setOverallBooking($data['overall_booking']);
        $monthlyGeneralInfo->setOverallAmount($data['overall_amount']);
        $monthlyGeneralInfo->setDteCreated($data['dte_created']);
        return $monthlyGeneralInfo;
    }

    /**
     * @return array
     */
    public function findByInfoYearMonth()
    {
        $sql = '
            SELECT *
              FROM monthly_general_info
                  WHERE info_year_month = :info_year_month
                    ORDER BY info_year_month';

        $params = array(
            'info_year_month' => date("Y-m-") . '01',
        );

        $data = $this->app['db']->fetchAll($sql, $params);
        if ($data) {
            return $this->hydrate($data[0]);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function insertMonthlyGeneralInfoBy()
    {
        $sql = '
          INSERT INTO monthly_general_info
            SET info_year_month =:info_year_month,
                overall_booking =:overall_booking,
                overall_amount =:overall_amount';

        $params = array(
            'info_year_month' => $this->getInfoYearMonth(),
            'overall_booking' => $this->getOverallBooking(),
            'overall_amount' => $this->getOverallAmount()
        );

        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param array $params
     * @param array $searchParams
     * @return bool
     */
    public function updateGeneralInfoById($params = array(), $searchParams = array())
    {
        $sql = "UPDATE monthly_general_info SET ";
        $sqlSetUpdates = null;
        foreach ($params as $key => $value) {
            $sqlSetUpdates .= ' '.$key .'=:'.$key .',';
        }

        $sql.= substr($sqlSetUpdates, 0, -1);
        $sql.= ' WHERE id = :id';

        $params  = array_merge($params, $searchParams);
        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $sql = '
            SELECT *
              FROM monthly_general_info
                ORDER BY info_year_month ASC';


        $data = $this->app['db']->fetchAll($sql);

        $generalInfos = array();

        foreach ($data as $generalInfo) {
            $generalInfos[] = $this->hydrate($generalInfo);
        }

        return $generalInfos;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
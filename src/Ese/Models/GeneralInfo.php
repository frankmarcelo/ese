<?php
/**
 * File: GeneralInfo.php
 *
 * PHP version 5.4
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Models;

use Silex\Application;

/**
 * Class GeneralInfo
 * This script is the main routing interface to bootstrap
 *
 * Class GeneralInfo
 *
 * @category Models
 * @package  Ese\Models
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class GeneralInfo implements \Serializable
{
    private $app;
    protected $id;
    protected $totalUsers;
    protected $totalBookings;
    protected $totalBookingsAmount;
    protected $totalConfirmedBookings;
    protected $totalBoxRequests;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set the user ID.
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the user ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $totalUsers
     */
    public function setTotalUsers($totalUsers)
    {
        $this->totalUsers = $totalUsers;
    }

    /**
     * @return mixed
     */
    public function getTotalUsers()
    {
        return $this->totalUsers;
    }

    /**
     * @param $totalBookings
     */
    public function setTotalBookings($totalBookings)
    {
        $this->totalBookings = $totalBookings;
    }

    /**
     * @return mixed
     */
    public function getTotalBookings()
    {
        return $this->totalBookings;
    }

    /**
     * @param $totalConfirmedBookings
     */
    public function setTotalConfirmedBookings($totalConfirmedBookings)
    {
        $this->totalConfirmedBookings = $totalConfirmedBookings;
    }

    /**
     * @return mixed
     */
    public function getTotalConfirmedBookings()
    {
        return $this->totalConfirmedBookings;
    }

    /**
     * @param $totalBookingsAmount
     */
    public function setTotalBookingsAmount($totalBookingsAmount)
    {
        $this->totalBookingsAmount = $totalBookingsAmount;
    }

    /**
     * @return mixed
     */
    public function getTotalBookingsAmount()
    {
        return $this->totalBookingsAmount;
    }

    /**
     * @param $totalBoxRequests
     */
    public function setTotalBoxRequests($totalBoxRequests)
    {
        $this->totalBoxRequests = $totalBoxRequests;
    }

    /**
     * @return mixed
     */
    public function getTotalBoxRequests()
    {
        return $this->totalBoxRequests;
    }

    /**
     * @param array $data
     * @return GeneralInfo
     */
    protected function hydrate(array $data)
    {
        $generalInfo = new GeneralInfo($this->app);
        $generalInfo->setId($data['id']);
        $generalInfo->setTotalUsers($data['total_users']);
        $generalInfo->setTotalBookings($data['total_bookings']);
        $generalInfo->setTotalBookingsAmount($data['total_bookings_amount']);
        $generalInfo->setTotalConfirmedBookings($data['total_confirmed_bookings']);
        return $generalInfo;
    }

    /**
     * @return GeneralInfo
     */
    public function findAll()
    {
        $sql = '
            SELECT *
              FROM general_info';

        $data = $this->app['db']->fetchAll($sql);
        return $this->hydrate($data[0]);
    }

    /**
     * @param $id
     * @return GeneralInfo
     */
    public function findById($id)
    {
        $sql = '
            SELECT *
              FROM general_info
                WHERE id = :id';

        $params = array(
            'id' => $id
        );
        $data = $this->app['db']->fetchAll($sql, $params);

        return $this->hydrate($data[0]);
    }

    /**
     * @param array $params
     * @param array $searchParams
     * @return bool
     */
    public function updateGeneralInfoById($params = array(), $searchParams = array())
    {
        $sql = "UPDATE general_info SET ";
        $sqlSetUpdates = null;
        foreach ($params as $key => $value) {
            $sqlSetUpdates .= ' '.$key .'=:'.$key .',';
        }

        $sql.= substr($sqlSetUpdates, 0, -1);
        $sql.= ' WHERE id = :id';

        $params  = array_merge($params, $searchParams);
        if ($this->app['db']->executeUpdate($sql, $params)) {
            return true;
        }
        return false;
    }

    /**
     * The Symfony Security component stores a serialized User object in the session.
     * We only need it to store the user ID, because the user provider's refreshUser() method is called on each request
     * and reloads the user by its ID.
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
}
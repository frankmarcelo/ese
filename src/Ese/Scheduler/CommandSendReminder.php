<?php
/**
 * File: CommandSendReminder.php
 *
 * PHP version 5.4
 *
 * @category Commands
 * @package  Ese\Scheduler
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Scheduler;

use Silex\Application;

/**
 * Class CommandSendReminder
 * This script is the main routing interface to bootstrap
 * instead being called upon on routing.php.
 * All http requests will come to this routing definitions
 * otherwise if not found it will render a not found.
 *
 * Class CommandSendReminder
 * @package  Ese\Scheduler
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 * @codeCoverageIgnore
 */
class CommandSendReminder
{
    /**
     * Generate the engagement badges and save it to cache
     *
     * @param Application $app bootstrap Silex app
     *
     * @return mixed
     */
    public static function generate(Application $app)
    {

    }
}
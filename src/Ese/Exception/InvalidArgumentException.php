<?php
/**
 * File: InvalidArgumentException.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

/**
 * Class InvalidArgumentException
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class InvalidArgumentException extends
    \InvalidArgumentException implements
    EseExceptionInterface
{
}

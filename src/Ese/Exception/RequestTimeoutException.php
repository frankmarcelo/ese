<?php
/**
 * File: RequestTimeoutException.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

/**
 * Class RequestTimeoutException
 * Your socket connection to the server was not
 * read from or written to within the timeout period.
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class RequestTimeoutException extends EseException
{
}

<?php
/**
 * File: ServiceResponseException.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ServiceResponseException
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class ServiceResponseException extends RuntimeException
{
    /**
     * @var Response Response
     */
    protected $response;

    /**
     * @var string Request ID
     */
    protected $requestId;

    /**
     * @var string Exception type (client / server)
     */
    protected $exceptionType;

    /**
     * @var string Exception code
     */
    protected $exceptionCode;

    /**
     * Set the exception code
     *
     * @param integer $code the response code when exception is thrown
     *
     * @return $this
     */
    public function setExceptionCode($code)
    {
        $this->exceptionCode = $code;

        return $this;
    }

    /**
     * Get the exception code
     *
     * @return string|null
     */
    public function getExceptionCode()
    {
        return $this->exceptionCode;
    }

    /**
     * Set the exception type
     *
     * @param string $type Exception type
     *
     * @return $this
     */
    public function setExceptionType($type)
    {
        $this->exceptionType = $type;

        return $this;
    }

    /**
     * Get the exception type (one of client or server)
     *
     * @return string|null
     */
    public function getExceptionType()
    {
        return $this->exceptionType;
    }

    /**
     * Set the request ID
     *
     * @param string $id Request ID
     *
     * @return $this
     */
    public function setRequestId($id)
    {
        $this->requestId = $id;

        return $this;
    }

    /**
     * Get the Request ID
     *
     * @return string|null
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set the associated response
     *
     * @param Response $response Response
     *
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get the associated response object
     *
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Get the status code of the response
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->response ? $this->response->getStatusCode() : null;
    }

    /**
     * Cast to a string
     *
     * @return string
     */
    public function __toString()
    {
        $message = get_class($this) . ': '
            . 'Cyberu Error Code: ' . $this->getExceptionCode() . ', '
            . 'Status Code: ' . $this->getStatusCode() . ', '
            . 'Cyberu Request ID: ' . $this->getRequestId() . ', '
            . 'Cyberu Error Type: ' . $this->getExceptionType() . ', '
            . 'Cyberu Error Message: ' . $this->getMessage();

        // Add the User-Agent if available
        if ($this->response && $this->response->getRequest()) {
            $message .= ', ' . 'User-Agent: ' .
                $this->response->getRequest()->getHeader('User-Agent');
        }

        return $message;
    }
}

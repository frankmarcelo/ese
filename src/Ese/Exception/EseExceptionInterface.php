<?php
/**
 * File: EseExceptionInterface.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

/**
 * Class EseExceptionInterface
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
interface EseExceptionInterface
{
    /**
     * Get the code error
     *
     * @return mixed
     */
    public function getCode();

    /**
     * Get the line error
     *
     * @return mixed
     */
    public function getLine();

    /**
     * Get the file error
     *
     * @return mixed
     */
    public function getFile();

    /**
     * Get the error message
     *
     * @return mixed
     */
    public function getMessage();

    /**
     * Get the previous line
     *
     * @return mixed
     */
    public function getPrevious();

    /**
     * Get the stack trace error
     *
     * @return mixed
     */
    public function getTrace();
}

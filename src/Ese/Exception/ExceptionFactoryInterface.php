<?php
/**
 * File: ExceptionFactoryInterface.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExceptionFactoryInterface
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
interface ExceptionFactoryInterface
{
    /**
     * Get the response from thrown exception
     *
     * @param Response $response Unsuccessful response that was encountered
     *
     * @return \Exception|ExceptionInterface
     */
    public function fromResponse(Response $response);
}

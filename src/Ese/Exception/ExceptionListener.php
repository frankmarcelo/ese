<?php
/**
 * File: ExceptionListener.php
 *
 * PHP version 5.4
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Exception;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ExceptionListener
 *
 * @category Exception
 * @package  Ese\Exception
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class ExceptionListener implements EventSubscriberInterface
{
    /**
     * @var ExceptionFactoryInterface Factory used to create new exceptions
     */
    protected $factory;

    /**
     * Construct the exception listener and assign the factory
     *
     * @param ExceptionFactoryInterface $factory object
     */
    public function __construct(ExceptionFactoryInterface $factory)
    {
        $this->factory = $factory;

        return $this;
    }

    /**
     * Get the listener events on request error
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array('request.error' => array('onRequestError', -1));
    }

    /**
     * Throws a more meaningful request exception if available
     *
     * @param Event $event Event emitted
     *
     * @throws ExceptionInterface|\Exception
     *
     * @return void
     */
    public function onRequestError($event)
    {
        $e = $this->factory->fromResponse($event['response']);
        $event->stopPropagation();
        throw $e;
    }
}

<?php
/**
 * File: EseControllerProvider.php
 *
 * PHP version 5.4
 *
 * @category Controllers
 * @package  Ese\Controllers
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Controllers;

use Ese\Exception\AccessDeniedException;
use Ese\Models\BookingItems;
use Ese\Models\Category;
use Ese\Models\GeneralInfo;
use Ese\Models\MonthlyGeneralInfo;
use Ese\Models\RequestBox;
use Ese\Models\SmsLogs;
use Ese\Models\Zone;
use Ese\Utils\Msisdn;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SimpleUser\UserManager;
use Ese\Models\ShipmentSchedule;
use Ese\Models\Booking;
use Ese\Models\BoxLocation;
use Ese\Models\BoxPricing;
use Ese\Models\BoxType;
use Ese\Models\Location;

/**
 * Class EseControllerProvider
 * This script is the main routing interface to bootstrap
 * instead being called upon on routing.php.
 * All http requests will come to this routing definitions
 * otherwise if not found it will render a not found.
 *
 * Class EseControllerProvider
 *
 * @category Controller
 * @package  Ese\Controllers
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 * @codeCoverageIgnore
 */
class EseControllerProvider implements ControllerProviderInterface
{
    /**
     * Define the controllers to match use as bootstrap
     *
     * @param Application $app bootstrap Silex app
     *
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $app['monolog']->addInfo(
            'Bootstrap connect before matching.'
        );

        $controllers->match(
            '/',
            function (Application $app) {

                $app['monolog']->addInfo('Render the index home page.');
                $isWelcomed = 0;

                $generalInfo = new GeneralInfo($app);
                $monthlyInfo = new MonthlyGeneralInfo($app);

                if ($app['user']) {
                    $isWelcomed = $app['user']->getIsWelcomed();
                }

                $response = new Response(
                    $app['twig']->render(
                        'index.twig',
                        array(
                            'isWelcomed' => $isWelcomed,
                            'page' => '/',
                            'user' => $app['user'],
                            'generalInfo' => $generalInfo->findAll(),
                            'monthlyInfo' => $monthlyInfo->findAll()
                        )
                    )
                );

                $response->setMaxAge(0);
                $response->setPublic();
                $response
                    ->headers
                    ->addCacheControlDirective('must-revalidate', true);

                return $response;
            }
        )
        ->method('GET')
        ->bind('HOME');

        $controllers->match(
            '/register',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are already logged in.'
                    );
                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }

                return $app->redirect(
                    $app["url_generator"]->generate("user.register")
                );
            }
        )
        ->method('GET')
        ->bind('USER.REDIRECT.REGISTRATION');

        $controllers->match(
            '/balik-bayan-boxes',
            function (Application $app, Request $request) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $location = new Location($app);
                    $boxPricing = new BoxPricing($app);
                    $boxLocation = new BoxLocation($app);
                    $boxType = new BoxType($app);
                    $categories = new Category($app);
                    $zone = new Zone($app);
                    $booking = null;
                    $bookingItem = null;
                    $errors = array();

                    if ($request->isMethod('POST')) {

                        $id = $session->getUser()->getId();
                        $params = $request->request->all();

                        $bookingPricing = new BoxPricing($app);
                        $pricing = $bookingPricing->findByBoxTypeIdAndBoxLocationId(
                            $request->request->get('boxTypeId'),
                            $request->request->get('boxLocationId')
                        );

                        $location = new Location($app);
                        $locationInfo = $location->findById($request->request->get('locationId'));
                        list($day, $month, $year) = explode("/", $request->request->get('pickUpDate'));
                        $pickUpDate = $year . '-' . $month . '-' . $day ;

                        $estimatedAmount = 0;
                        if ($request->request->get('name')) {
                            $names = $request->request->get('name');
                            $prices = $request->request->get('price');
                            foreach ($names as $key => $name) {
                                if ($key > 0) {
                                    $estimatedAmount += $prices[$key];
                                }
                            }
                        }

                        $shipmentDate = date("Y-m", strtotime($pickUpDate . " +1 month")) .'-01';
                        $booking = new Booking($app);
                        $booking->setUserId($id);
                        $booking->setBoxPricingId($pricing->getId());
                        $booking->setBoxTypeId($request->request->get('boxTypeId'));
                        $booking->setPnzCode($request->request->get('pnzCode'));
                        $booking->setShipmentDate($shipmentDate);
                        $booking->setPickUpDate($pickUpDate);
                        $booking->setDeliveryName($request->request->get('deliveryName'));
                        $booking->setDeliveryAddress($request->request->get('deliveryAddress'));
                        $booking->setDeliveryEmail($request->request->get('deliveryEmail'));
                        $booking->setDeliveryPhone(Msisdn::convert($request->request->get('deliveryPhone')));
                        $booking->setSenderName($request->request->get('senderName'));
                        $booking->setSenderAddress($request->request->get('senderAddress') . 'New Zealand');
                        $booking->setSenderEmail($request->request->get('senderEmail'));
                        $booking->setSenderPhone(Msisdn::convert($request->request->get('senderPhone')));
                        $booking->setEstimatedAmount($estimatedAmount);
                        $booking->setAdditionalCharges($locationInfo->getPickUpExtraCharge());
                        $booking->setAmount($pricing->getAmount() + $locationInfo->getPickUpExtraCharge());
                        if ($request->request->get('boxTypeId') == 1){
                            $booking->setIsJumboBox($request->request->get('boxTypeId'));
                        } else {
                            $booking->setIsJumboBox(0);
                        }

                        if (array_key_exists('save-and-request-pickup', $params)) {
                            $booking->setIsPickedUp(0);
                        } else {
                            $booking->setIsPickedUp(2);
                        }
                        $booking->setRemarks($request->request->get('remarks'));
                        $booking->setDteCreated(date("Y-m-d H:i:s"));

                        if ($booking->validate($request)) {

                            $bookingId = $booking->save($booking);

                            if ($request->request->get('name')) {
                                $names = $request->request->get('name');
                                $categories = $request->request->get('categoryId');
                                $prices = $request->request->get('price');
                                $quantities = $request->request->get('quantity');

                                foreach ($names as $key => $name) {
                                    if (strlen($categories[$key])> 0 &&
                                        strlen($names[$key])> 0 &&
                                        strlen($prices[$key])> 0 &&
                                        strlen($quantities[$key])> 0
                                    ) {
                                        $bookingItem = new BookingItems($app);
                                        $bookingItem->setBookingId($bookingId);
                                        $bookingItem->setCategoryId($categories[$key]);
                                        $bookingItem->setName($names[$key]);
                                        $bookingItem->setAmount($prices[$key]);
                                        $bookingItem->setQuantity($quantities[$key]);
                                        $bookingItem->setDteCreated(date("Y-m-d H:i:s"));
                                        $bookingItem->save($bookingItem);
                                    }
                                }
                            }

                            if (array_key_exists('save-and-request-pickup', $params)) {

                                $cache = $app['doctrine.cache'];
                                if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                                    $smsSessionId = $app['sms.request']
                                        ->getCommand('Auth')
                                        ->execute()
                                        ->getSessionId();
                                    $cache->save(
                                        $app['cache']['key'],
                                        $smsSessionId,
                                        $app['cache']['lifetime']
                                    );
                                }

                                $app['smsSessionId'] = $smsSessionId;
                                $smsMobile = $app['clickatell']['CLICKATELL_MOBILE'];
                                $smsMessage = 'New box for pick up with code '
                                    . $request->request->get('pnzCode')
                                    . ' and pick up date of ' . date("l jS M Y",strtotime($pickUpDate));
                                $app['sms.request']
                                    ->getCommand('SendMsg', array(
                                            'to' => $smsMobile,
                                            'text' => $smsMessage,
                                            'session_id' => $app['smsSessionId']
                                        )
                                    )
                                    ->execute();

                                $smsLogs = new SmsLogs($app);
                                $smsLogs->setSmsMessage($smsMessage);
                                $smsLogs->setMobile($smsMobile);
                                $smsLogs->logSmsMessage();

                                $generalInfo = new GeneralInfo($app);
                                $info = $generalInfo->findById(1);
                                $generalInfo->updateGeneralInfoById(
                                    array(
                                        'total_bookings' =>
                                        $info->getTotalBookings() + 1
                                    ),
                                    array(
                                        'id' => $info->getId()
                                    )
                                );
                            }

                            $app['session']->getFlashBag()->set(
                                'alert',
                                'Create box status successful.'
                            );

                            return $app->redirect(
                                $app['url_generator']->generate('USER.DELIVERY.HISTORY')
                            );

                        } else {

                            $errors = $booking->getErrors();
                            $booking->setPickUpDate($request->request->get('pickUpDate'));
                            $booking->setShipmentDate(strtotime($shipmentDate));
                        }
                    }

                    $userLocation = $location->findByUserId(
                        $session->getUser()->getId()
                    );

                    $zoneSchedule = $zone->findByZoneId(
                        $userLocation->getZoneId()
                    );

                    $dateStart = strtotime("now");
                    $dateEnd = strtotime(date("Y-m-t", strtotime("+3 months")));
                    $datesEnabled = [];

                    while ($dateStart <= $dateEnd) {

                        if (
                           $zoneSchedule['zoneSchedule']->getIsMonday() == 1 &&
                           date('D', $dateStart) === 'Mon'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsTuesday() == 1 &&
                            date('D', $dateStart) === 'Tue'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsWednesday() == 1 &&
                            date('D', $dateStart) === 'Wed'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsThursday() == 1 &&
                            date('D', $dateStart) === 'Thu'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsFriday() == 1 &&
                            date('D', $dateStart) === 'Fri'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsSaturday() == 1 &&
                            date('D', $dateStart) === 'Sat'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        } elseif (
                            $zoneSchedule['zoneSchedule']->getIsSunday() == 1 &&
                            date('D', $dateStart) === 'Sun'
                        ) {
                            $datesEnabled[] = date("Y-m-d", $dateStart);
                        }

                        $dateStart = strtotime("+1 day", $dateStart);
                    }

                    $response = new Response(
                        $app['twig']->render(
                            'my-deliveries.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'deliveries',
                                'location' => $userLocation,
                                'datesEnabled' => $datesEnabled,
                                'booking' => $booking,
                                'pickUpDate' => date("Y-m-d", strtotime("+2 days")),
                                'errors' => implode("\n", $errors),
                                'boxPricings' => $boxPricing->findAll(),
                                'boxLocations' => $boxLocation->findAll(),
                                'boxTypes' => $boxType->findAll(),
                                'categories' => $categories->findAll()
                            )
                        )
                    );

                    $response->setMaxAge(0);
                    $response->setPublic();
                    $response
                        ->headers
                        ->addCacheControlDirective(
                            'must-revalidate', true
                        );

                    return $response;

                } else {
                    $app['session']
                        ->getFlashBag()->set(
                            'alert',
                            'You are not allowed to view this page.'
                        );

                    return $app->redirect(
                        $app['url_generator']->generate('user.logout')
                    );
                }
            }
        )
        ->bind('USER.MY_DELIVERIES');

        $controllers->match(
            '/balik-bayan-boxes/{id}/edit',
            function (Application $app, Request $request, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $location = new Location($app);
                    $boxPricing = new BoxPricing($app);
                    $boxLocation = new BoxLocation($app);
                    $boxType = new BoxType($app);
                    $categories = new Category($app);
                    $zone = new Zone($app);
                    $bookingInfo = new Booking($app);
                    $booking = $bookingInfo->findByPnzCode($id);
                    $bookingItem = null;
                    $errors = array();

                    if ($booking->getIsPickedUp() !== 0 &&
                        $booking->getIsPickedUp() !== 1 &&
                        $booking->getUserId() == $session->getUser()->getId()
                    ) {

                        if ($request->isMethod('POST')) {
                            $id = $session->getUser()->getId();
                            $params = $request->request->all();

                            $bookingPricing = new BoxPricing($app);
                            $pricing = $bookingPricing->findByBoxTypeIdAndBoxLocationId(
                                $request->request->get('boxTypeId'),
                                $request->request->get('boxLocationId')
                            );

                            $location = new Location($app);
                            $locationInfo = $location->findById($request->request->get('locationId'));
                            list($day, $month, $year) = explode("/", $request->request->get('pickUpDate'));
                            $pickUpDate = $year . '-' . $month . '-' . $day ;

                            $estimatedAmount = 0;
                            if ($request->request->get('name')) {
                                $names = $request->request->get('name');
                                $prices = $request->request->get('price');
                                foreach ($names as $key => $name) {
                                    if ($key > 0) {
                                        $estimatedAmount += $prices[$key];
                                    }
                                }
                            }

                            $shipmentDate = date("Y-m", strtotime($pickUpDate . " +1 month")) .'-01';
                            $bookingId = $booking->getId();

                            $booking->setBoxPricingId($pricing->getId());
                            $booking->setBoxTypeId($request->request->get('boxTypeId'));
                            $booking->setShipmentDate($shipmentDate);
                            $booking->setPickUpDate($pickUpDate);
                            $booking->setDeliveryName($request->request->get('deliveryName'));
                            $booking->setDeliveryAddress($request->request->get('deliveryAddress'));
                            $booking->setDeliveryEmail($request->request->get('deliveryEmail'));
                            $booking->setDeliveryPhone(Msisdn::convert($request->request->get('deliveryPhone')));
                            $booking->setSenderName($request->request->get('senderName'));
                            $booking->setSenderAddress($request->request->get('senderAddress') . ' New Zealand');
                            $booking->setSenderEmail($request->request->get('senderEmail'));
                            $booking->setSenderPhone(Msisdn::convert($request->request->get('senderPhone')));
                            $booking->setEstimatedAmount($estimatedAmount);
                            $booking->setAdditionalCharges($locationInfo->getPickUpExtraCharge());
                            $booking->setAmount($pricing->getAmount() + $locationInfo->getPickUpExtraCharge());
                            if ($request->request->get('boxTypeId') == 1){
                                $booking->setIsJumboBox($request->request->get('boxTypeId'));
                            } else {
                                $booking->setIsJumboBox(0);
                            }

                            if (array_key_exists('save-and-request-pickup', $params)) {
                                $booking->setIsPickedUp(0);
                            } else {
                                $booking->setIsPickedUp(2);
                            }
                            $booking->setRemarks($request->request->get('remarks'));
                            $booking->setDteUpdated();

                            if ($booking->validate($request, $isEdit = true, $id)) {

                                $booking->update($booking, $bookingId);

                                if ($request->request->get('name')) {
                                    $names = $request->request->get('name');
                                    $categories = $request->request->get('categoryId');
                                    $prices = $request->request->get('price');
                                    $quantities = $request->request->get('quantity');

                                    $deleteBookingItems = new BookingItems($app);
                                    $deleteBookingItems->deleteBookingItemByBookingId($bookingId);

                                    foreach ($names as $key => $name) {
                                        if (strlen($categories[$key])> 0 &&
                                            strlen($names[$key])> 0 &&
                                            strlen($prices[$key])> 0 &&
                                            strlen($quantities[$key])> 0
                                        ) {
                                            $bookingItem = new BookingItems($app);
                                            $bookingItem->setBookingId($bookingId);
                                            $bookingItem->setCategoryId($categories[$key]);
                                            $bookingItem->setName($names[$key]);
                                            $bookingItem->setAmount($prices[$key]);
                                            $bookingItem->setQuantity($quantities[$key]);
                                            $bookingItem->setDteCreated(date("Y-m-d H:i:s"));
                                            $bookingItem->save($bookingItem);
                                        }
                                    }
                                }

                                if (array_key_exists('save-and-request-pickup', $params)) {

                                    $cache = $app['doctrine.cache'];
                                    if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                                        $smsSessionId = $app['sms.request']
                                            ->getCommand('Auth')
                                            ->execute()
                                            ->getSessionId();
                                        $cache->save(
                                            $app['cache']['key'],
                                            $smsSessionId,
                                            $app['cache']['lifetime']
                                        );
                                    }

                                    $app['smsSessionId'] = $smsSessionId;
                                    $smsMobile = Msisdn::convert($app['clickatell']['CLICKATELL_MOBILE']);
                                    $smsMessage = 'New box for pick up with code '
                                        . $request->request->get('pnzCode')
                                        . ' and pick up date of ' . date("l jS M Y",strtotime($pickUpDate));
                                    $app['sms.request']
                                        ->getCommand('SendMsg', array(
                                                'to' => $smsMobile,
                                                'text' => $smsMessage,
                                                'session_id' => $app['smsSessionId']
                                            )
                                        )
                                        ->execute();

                                    $smsLogs = new SmsLogs($app);
                                    $smsLogs->setSmsMessage($smsMessage);
                                    $smsLogs->setMobile($smsMobile);
                                    $smsLogs->logSmsMessage();

                                    $generalInfo = new GeneralInfo($app);
                                    $info = $generalInfo->findById(1);
                                    $generalInfo->updateGeneralInfoById(
                                        array(
                                            'total_bookings' =>
                                            $info->getTotalBookings() + 1
                                        ),
                                        array(
                                            'id' => $info->getId()
                                        )
                                    );
                                }

                                $app['session']->getFlashBag()->set(
                                    'alert',
                                    'Update box status successful.'
                                );

                                return $app->redirect(
                                    $app['url_generator']->generate('USER.DELIVERY.HISTORY')
                                );

                            } else {

                                $errors = $booking->getErrors();
                                $booking->setPickUpDate($request->request->get('pickUpDate'));
                                $booking->setShipmentDate(strtotime($shipmentDate));
                            }
                        }

                        $userLocation = $location->findByUserId(
                            $session->getUser()->getId()
                        );

                        $zoneSchedule = $zone->findByZoneId(
                            $userLocation->getZoneId()
                        );

                        $dateStart = strtotime("now");
                        $dateEnd = strtotime(date("Y-m-t", strtotime("+3 months")));
                        $datesEnabled = [];

                        while ($dateStart <= $dateEnd) {

                            if (
                                $zoneSchedule['zoneSchedule']->getIsMonday() == 1 &&
                                date('D', $dateStart) === 'Mon'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsTuesday() == 1 &&
                                date('D', $dateStart) === 'Tue'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsWednesday() == 1 &&
                                date('D', $dateStart) === 'Wed'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsThursday() == 1 &&
                                date('D', $dateStart) === 'Thu'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsFriday() == 1 &&
                                date('D', $dateStart) === 'Fri'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsSaturday() == 1 &&
                                date('D', $dateStart) === 'Sat'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            } elseif (
                                $zoneSchedule['zoneSchedule']->getIsSunday() == 1 &&
                                date('D', $dateStart) === 'Sun'
                            ) {
                                $datesEnabled[] = date("Y-m-d", $dateStart);
                            }

                            $dateStart = strtotime("+1 day", $dateStart);
                        }

                        $boxPricingInfo = $boxPricing->findById($booking->getBoxPricingId());

                        $response = new Response(
                            $app['twig']->render(
                                'my-deliveries-edit.twig',
                                array(
                                    'isWelcomed' => $app['user']->getIsWelcomed(),
                                    'page' => 'deliveries',
                                    'location' => $userLocation,
                                    'datesEnabled' => $datesEnabled,
                                    'booking' => $booking,
                                    'pickUpDate' => $booking->getPickUpDate(),
                                    'boxLocationId' => $boxPricingInfo->getBoxLocationId(),
                                    'errors' => implode("\n", $errors),
                                    'boxPricings' => $boxPricing->findAll(),
                                    'boxLocations' => $boxLocation->findAll(),
                                    'boxTypes' => $boxType->findAll(),
                                    'categories' => $categories->findAll()
                                )
                            )
                        );

                        $response->setMaxAge(0);
                        $response->setPublic();
                        $response
                            ->headers
                            ->addCacheControlDirective(
                                'must-revalidate', true
                            );

                        return $response;

                    } else {

                        $app['session']
                            ->getFlashBag()->set(
                                'alert',
                                'You are not allowed to view this page.'
                            );

                        return $app->redirect(
                            $app['url_generator']->generate('user.logout')
                        );
                    }

                } else {
                    $app['session']
                        ->getFlashBag()->set(
                            'alert',
                            'You are not allowed to view this page.'
                        );

                    return $app->redirect(
                        $app['url_generator']->generate('user.logout')
                    );
                }
            }
        )
        ->bind('USER.MY_DELIVERIES.EDIT');

        $controllers->match(
            '/contact-us',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {

                    $response = new Response(
                        $app['twig']->render(
                            'contact-us.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'contact'
                            )
                        )
                    );

                    $response->setTtl(10);
                    return $response;

                } else {

                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );

                }
            }
        )
        ->method('GET')
        ->bind('USER.CONTACT.US');

        $controllers->match(
            '/export-declaration-details/{id}-info.html',
            function (Application $app, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    || $app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $booking = new Booking($app);
                    $bookingInfo = $booking->findByPnzCode($id);

                    if (($session->getUser()->getId() === $bookingInfo->getUserId())
                        || $app['security']->isGranted('ROLE_ADMIN')
                    ) {
                        $response = new Response(
                            $app['twig']->render(
                                'box-information.twig',
                                array(
                                    'isWelcomed' => $app['user']->getIsWelcomed(),
                                    'page' => 'pickup',
                                    'booking' => $bookingInfo
                                )
                            )
                        );

                        $response->setMaxAge(0);
                        $response->setPublic();
                        $response
                            ->headers
                            ->addCacheControlDirective('must-revalidate', true);

                        return $response;

                    } else {

                        $app['session']->getFlashBag()->set(
                            'alert',
                            'You are not allowed to view this page.'
                        );

                        return $app->redirect(
                            $app['url_generator']->generate('HOME')
                        );

                    }

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.BOX.DETAILS');


        $controllers->match(
            '/export-declaration-details/{id}-info-public.html',
            function (Application $app, $id) use ($app) {

                $booking = new Booking($app);
                $response = new Response(
                    $app['twig']->render(
                        'export-confirmation-email.twig',
                        array(
                            'page' => 'pickup',
                            'booking' => $booking->findByPnzCode($id)
                        )
                    )
                );

                $response->setMaxAge(0);
                $response->setPublic();
                $response
                    ->headers
                    ->addCacheControlDirective('must-revalidate', true);

                return $response;

            }
        )
        ->method('GET')
        ->bind('USER.BOX.DETAILS.PUBLIC');

        $controllers->match(
            '/pick-ups',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && $app['security']->isGranted('ROLE_ADMIN')
                ) {
                    $booking = new Booking($app);
                    $pendingBookings = array();
                    if ($bookings = $booking->findAllBookingByMonth(0, 0)) {
                        foreach ($bookings as $bookingInfo) {
                            $pendingBookings[$bookingInfo->getPickUpDate()][] = $bookingInfo;
                        }
                    }

                    $response = new Response(
                        $app['twig']->render(
                            'my-pickups.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'admin_pickup',
                                'confirmedBookings' => $booking->findAll($isPickedUp = 1),
                                'pendingBookings' => $pendingBookings
                            )
                        )
                    );

                    $response->setMaxAge(0);
                    $response->setPublic();
                    $response
                        ->headers
                        ->addCacheControlDirective('must-revalidate', true);

                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.PICK.UP');

        $controllers->match(
            '/box-requests',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && $app['security']->isGranted('ROLE_ADMIN')
                ) {
                    $boxRequests = new RequestBox($app);
                    $response = new Response(
                        $app['twig']->render(
                            'my-box-request-deliveries.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'box-requests',
                                'pendingBookings' => $boxRequests->findAll()
                            )
                        )
                    );

                    $response->setMaxAge(0);
                    $response->setPublic();
                    $response
                        ->headers
                        ->addCacheControlDirective('must-revalidate', true);

                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.BOX.REQUESTS');

        $controllers->match(
            '/box-request',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $boxType = new BoxType($app);
                    $response = new Response(
                        $app['twig']->render(
                            'request-box.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'box-request',
                                'boxTypes' => $boxType->findAll(),
                            )
                        )
                    );


                    $response->setMaxAge(0);
                    $response->setPublic();
                    $response
                        ->headers
                        ->addCacheControlDirective('must-revalidate', true);

                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.BOX.REQUEST');

        $controllers->match(
            '/box-requests/{id}',
            function (Application $app, Request $request, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {
                    list($day, $month, $year) = explode("/", $request->request->get('requestDate'));
                    $requestDate = $year . '-' . $month . '-' . $day ;

                    $requestBox = new RequestBox($app);
                    $requestBox->setUserId($id);
                    $requestBox->setBoxTypeId($request->request->get('boxTypeId'));
                    $requestBox->setRequestDate($requestDate);
                    $requestBox->setIsDelivered($isDelivered = 0);
                    $requestBox->setDteCreated(date("Y-m-d H:i:s"));

                    if ($requestBox->validate()) {
                        $requestBox->save($requestBox);

                        $cache = $app['doctrine.cache'];
                        if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                            $smsSessionId = $app['sms.request']
                                ->getCommand('Auth')
                                ->execute()
                                ->getSessionId();
                            $cache->save(
                                $app['cache']['key'],
                                $smsSessionId,
                                $app['cache']['lifetime']
                            );
                        }

                        $boxType = null;
                        if ($request->request->get('boxTypeId') == 1) {
                            $boxType = 'Jumbo';
                        } else {
                            $boxType = 'Regular';
                        }

                        $app['smsSessionId'] = $smsSessionId;
                        $smsMobile = Msisdn::convert($app['clickatell']['CLICKATELL_MOBILE']);
                        $smsMessage = 'New box request from ' . $app['user']->getName()
                            . ' box type ' . $boxType . ' '
                            . ' and drop off date ' .
                            date("l jS M Y",strtotime($requestDate)) . '.';
                        $app['sms.request']
                            ->getCommand('SendMsg', array(
                                    'to' => $smsMobile,
                                    'text' => $smsMessage,
                                    'session_id' => $app['smsSessionId']
                                )
                            )
                            ->execute();

                        $smsLogs = new SmsLogs($app);
                        $smsLogs->setSmsMessage($smsMessage);
                        $smsLogs->setMobile($smsMobile);
                        $smsLogs->logSmsMessage();

                        $app['smsSessionId'] = $smsSessionId;
                        $smsMobile = Msisdn::convert($app['user']->getMobile());
                        $smsMessage = 'This is to confirm that you requested a '. $boxType . ' box '
                            . ' and drop off date ' .
                            date("l jS M Y",strtotime($requestDate)) . '.';
                        $app['sms.request']
                            ->getCommand('SendMsg', array(
                                    'to' => $smsMobile,
                                    'text' => $smsMessage,
                                    'session_id' => $app['smsSessionId']
                                )
                            )
                            ->execute();

                        $smsLogs = new SmsLogs($app);
                        $smsLogs->setSmsMessage($smsMessage);
                        $smsLogs->setMobile($smsMobile);
                        $smsLogs->logSmsMessage();


                        $generalInfo = new GeneralInfo($app);
                        $info = $generalInfo->findById(1);
                        $generalInfo->updateGeneralInfoById(
                            array(
                                'total_box_requests' =>
                                $info->getTotalBoxRequests() + 1
                            ),
                            array(
                                'id' => $info->getId()
                            )
                        );

                        $app['session']->getFlashBag()->set(
                            'alert',
                            'Box request successful.'
                        );

                        return $app->redirect(
                            $app['url_generator']->generate('HOME')
                        );

                    } else {

                        $app['session']->getFlashBag()->set(
                            'alert',
                            'One of the required fields is empty.'
                        );

                        return $app->redirect(
                            $app['url_generator']->generate('USER.BOX.REQUEST')
                        );

                    }

                } else {

                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('POST')
        ->bind('USER.BOX.ADD.REQUESTS');

        $controllers->match(
            '/delivery-history',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {
                    $booking = new Booking($app);

                    $response = new Response(
                        $app['twig']->render(
                            'delivery-history.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'delivery_history',
                                'bookings' => $booking->findAllByUserId($session->getUser()->getId())
                            )
                        )
                    );

                    $response->setMaxAge(0);
                    $response->setPublic();
                    $response
                        ->headers
                        ->addCacheControlDirective('must-revalidate', true);

                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.DELIVERY.HISTORY');

        $controllers->match(
            '/shipment-schedule',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {

                    $shipmentSchedule = new ShipmentSchedule($app);

                    $response = new Response(
                        $app['twig']->render(
                            'shipment-schedule.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'shipment',
                                'schedules' => $shipmentSchedule->findAll()
                            )
                        )
                    );

                    $response->setTtl(10);
                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.SHIPMENT.SCHEDULE');

        $controllers->match(
            '/terms-and-conditions',
            function () use ($app) {
                $response = new Response(
                    $app['twig']->render(
                        'terms-and-conditions.twig',
                        array(
                            'page' => ''
                        )
                    )
                );

                $response->setMaxAge(0);
                $response->setPublic();
                $response
                    ->headers
                    ->addCacheControlDirective('must-revalidate', true);

                return $response;
            }
        )
        ->method('GET')
        ->bind('TERMS');

        $controllers->match(
            '/delivery-rates',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {

                    $boxType = new BoxType($app);
                    $boxPricing = new BoxPricing($app);
                    $location = new Location($app);
                    $response = new Response(
                        $app['twig']->render(
                            'delivery-rates.twig',
                            array(
                                'isWelcomed' => $app['user']->getIsWelcomed(),
                                'page' => 'delivery',
                                'boxTypes' => $boxType
                                    ->findAll(),
                                'boxPricings' => $boxPricing
                                    ->findAll(),
                                'extraCharges' => $location
                                    ->findAllLocationWithExtraCharges()
                            )
                        )
                    );
                    $response->setTtl(10);

                    return $response;

                } else {
                    $app['session']->getFlashBag()->set(
                        'alert',
                        'You are not allowed to view this page.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('HOME')
                    );
                }
            }
        )
        ->method('GET')
        ->bind('USER.DELIVERY.RATES');

        $controllers->match(
            '/user/welcomed',
            function () use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {
                    $userManager = new UserManager($app['db'], $app);
                    $user = $userManager
                        ->getUser(
                            $session->getUser()->getId()
                        );
                    $user->setIsWelcomed(1);
                    $userManager->update($user);
                }

                return new Response('ok', 200);
            }
        )
        ->method('POST')
        ->assert('id', '\d+')
        ->bind('USER.UPDATE.ISWELCOMED');

        $controllers->match(
            '/box-requests/{id}/update',
            function (Application $app, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {

                    $requestBox = new RequestBox($app);
                    $requestBox->setId($id);
                    $requestBox->update($requestBox);

                    $app['session']->getFlashBag()->set(
                        'alert',
                        'Box request update successful.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('USER.BOX.REQUESTS')
                    );
                }
            }
        )
        ->method('POST')
        ->assert('id', '\d+')
        ->bind('USER.BOX_REQUEST.UPDATE');

        $controllers->match(
            '/box-create/{id}',
            function (Application $app, Request $request, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session && $session->getUser()->getId()) {
                    $params = $request->request->all();

                    $bookingPricing = new BoxPricing($app);
                    $pricing = $bookingPricing->findByBoxTypeIdAndBoxLocationId(
                        $request->request->get('boxTypeId'),
                        $request->request->get('boxLocationId')
                    );

                    $location = new Location($app);
                    $locationInfo = $location->findById($request->request->get('locationId'));
                    list($day, $month, $year) = explode("/", $request->request->get('pickUpDate'));
                    $pickUpDate = $year . '-' . $month . '-' . $day ;

                    $estimatedAmount = 0;
                    if ($request->request->get('name')) {
                        $names = $request->request->get('name');
                        $prices = $request->request->get('price');
                        foreach ($names as $key => $name) {
                            if ($key > 0) {
                                $estimatedAmount += $prices[$key];
                            }
                        }
                    }

                    $booking = new Booking($app);
                    $booking->setUserId($id);
                    $booking->setBoxPricingId($pricing->getId());
                    $booking->setBoxTypeId($request->request->get('boxTypeId'));
                    $booking->setPnzCode(strtoupper($request->request->get('pnzCode')));
                    $booking->setPickUpDate($pickUpDate);
                    $booking->setDeliveryName($request->request->get('deliveryName'));
                    $booking->setDeliveryAddress($request->request->get('deliveryAddress'));
                    $booking->setDeliveryEmail($request->request->get('deliveryEmail'));
                    $booking->setDeliveryPhone(Msisdn::convert($request->request->get('deliveryPhone')));
                    $booking->setSenderName($request->request->get('senderName'));
                    $booking->setSenderAddress($request->request->get('senderAddress') . 'New Zealand');
                    $booking->setSenderEmail($request->request->get('senderEmail'));
                    $booking->setSenderPhone(Msisdn::convert($request->request->get('senderPhone')));
                    $booking->setEstimatedAmount($estimatedAmount);
                    $booking->setAdditionalCharges($locationInfo->getPickUpExtraCharge());
                    $booking->setAmount($pricing->getAmount() + $locationInfo->getPickUpExtraCharge());
                    if ($request->request->get('boxTypeId') == 1){
                        $booking->setIsJumboBox($request->request->get('boxTypeId'));
                    } else {
                        $booking->setIsJumboBox(0);
                    }

                    if (array_key_exists('save-and-request-pickup', $params)) {
                        $booking->setIsPickedUp(0);
                    } else {
                        $booking->setIsPickedUp(2);
                    }
                    $booking->setRemarks($request->request->get('remarks'));
                    $booking->setDteCreated(date("Y-m-d H:i:s"));

                    if ($booking->validate($request)) {

                        $bookingId = $booking->save($booking);

                        if ($request->request->get('name')) {
                            $names = $request->request->get('name');
                            $categories = $request->request->get('categoryId');
                            $prices = $request->request->get('price');
                            $quantities = $request->request->get('quantity');

                            foreach ($names as $key => $name) {
                                if ($key > 0) {
                                    $bookingItem = new BookingItems($app);
                                    $bookingItem->setBookingId($bookingId);
                                    $bookingItem->setCategoryId($categories[$key]);
                                    $bookingItem->setName($names[$key]);
                                    $bookingItem->setAmount($prices[$key]);
                                    $bookingItem->setQuantity($quantities[$key]);
                                    $bookingItem->setDteCreated(date("Y-m-d H:i:s"));
                                    $bookingItem->save($bookingItem);
                                }
                            }
                        }

                        if (array_key_exists('save-and-request-pickup', $params)) {

                            $cache = $app['doctrine.cache'];
                            if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                                $smsSessionId = $app['sms.request']
                                    ->getCommand('Auth')
                                    ->execute()
                                    ->getSessionId();
                                $cache->save(
                                    $app['cache']['key'],
                                    $smsSessionId,
                                    $app['cache']['lifetime']
                                );
                            }

                            $app['smsSessionId'] = $smsSessionId;
                            $smsMobile = $app['clickatell']['CLICKATELL_MOBILE'];
                            $smsMessage = 'New box for pick up with code '
                                . $request->request->get('pnzCode')
                                . ' and pick up date of ' . $pickUpDate ;
                            $app['sms.request']
                                ->getCommand('SendMsg', array(
                                        'to' => $smsMobile,
                                        'text' => $smsMessage,
                                        'session_id' => $app['smsSessionId']
                                    )
                                )
                                ->execute();

                            $smsLogs = new SmsLogs($app);
                            $smsLogs->setSmsMessage($smsMessage);
                            $smsLogs->setMobile($smsMobile);
                            $smsLogs->logSmsMessage();

                            $generalInfo = new GeneralInfo($app);
                            $info = $generalInfo->findById(1);
                            $generalInfo->updateGeneralInfoById(
                                array(
                                    'total_bookings' =>
                                        $info->getTotalBookings() + 1
                                ),
                                array(
                                    'id' => $info->getId()
                                )
                            );
                        }

                        $app['session']->getFlashBag()->set(
                            'alert',
                            'Create box status successful.'
                        );

                        return $app->redirect(
                            $app['url_generator']->generate('USER.DELIVERY.HISTORY')
                        );

                    } else {

                        $booking->getErrors();

                    }

                } else {

                    throw new AccessDeniedException();
                }
            }
        )
        ->method('POST')
        ->bind('USER.BOX.SEND');

        $controllers->match(
            '/box-status-update/{id}',
            function (Application $app, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && $app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $booking = new Booking($app);
                    $bookingInfo = $booking->findByPnzCode($id);
                    $bookingInfo->updateBookingStatusById($isPickedUp = 1);

                    $cache = $app['doctrine.cache'];
                    if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                        $smsSessionId = $app['sms.request']
                            ->getCommand('Auth')
                            ->execute()
                            ->getSessionId();
                        $cache->save(
                            $app['cache']['key'],
                            $smsSessionId,
                            $app['cache']['lifetime']
                        );
                    }

                    $smsMobile = Msisdn::convert($bookingInfo->getBookingUserMobile());

                    $app['smsSessionId'] = $smsSessionId;
                    $smsMessage =  strtoupper($bookingInfo->getPnzCode()).
                        ' has been picked up, please refer to your email for a'.
                        ' copy of consignment form.';

                    $app['sms.request']
                        ->getCommand('SendMsg', array(
                                'to' => $smsMobile,
                                'text' => $smsMessage,
                                'session_id' => $app['smsSessionId']
                            )
                        )
                        ->execute();

                    $smsLogs = new SmsLogs($app);
                    $smsLogs->setSmsMessage($smsMessage);
                    $smsLogs->setMobile($smsMobile);
                    $smsLogs->logSmsMessage();

                    $generalInfo = new GeneralInfo($app);
                    $info = $generalInfo->findById(1);
                    $generalInfo->updateGeneralInfoById(
                        array(
                            'total_confirmed_bookings' =>
                                $info->getTotalConfirmedBookings() + 1,
                            'total_bookings_amount' =>
                                $info->getTotalBookingsAmount() +
                                    $bookingInfo->getAmount(),
                        ),
                        array(
                            'id' => $info->getId()
                        )
                    );

                    $userManager = new UserManager($app['db'], $app);
                    $user = $userManager->getUser($bookingInfo->getUserId());
                    $userManager->updateTotalPickUps($user);

                    $emailSubject = 'Export Declaration Details for '.
                        strtoupper($bookingInfo->getPnzCode());

                    $app['mailer']->send(\Swift_Message::newInstance()
                        ->setSubject($emailSubject)
                        ->setFrom(array($app['swiftmailer.options']['username']))
                        ->setTo(array($user->getEmail()))
                        ->setBcc(array($app['swiftmailer.options']['username'],
                                $app['swiftmailer.options']['bcc'])
                        )
                        ->setBody($app['twig']->render('export-confirmation-email.twig',
                                array(
                                    'booking' => $bookingInfo
                                )
                            ),'text/html'
                        )
                    );

                    $app['session']->getFlashBag()->set(
                        'alert',
                        'Update box status successful.'
                    );

                    return $app->redirect(
                        $app['url_generator']->generate('USER.PICK.UP')
                    );

                } else {

                    throw new AccessDeniedException();
                }
            }
        )
        ->method('POST')
        ->bind('BOX.STATUS.UPDATE');

        $controllers->match(
            '/box-status-delete/{id}',
            function (Application $app, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $booking = new Booking($app);
                    $bookingItems = new BookingItems($app);
                    $bookingInfo = $booking->findByPnzCode($id);

                    if ($bookingInfo->getIsPickedUp() == 2 &&
                        $bookingInfo->getUserId() == $session->getUser()->getId()
                    ) {

                        if ($bookingInfo->deleteBookingByPnzCode($bookingInfo->getPnzCode())) {
                            $bookingItems->deleteBookingItemByBookingId(
                                $bookingInfo->getId()
                            );

                            $app['session']->getFlashBag()->set(
                                'alert',
                                'Box with code ' . $bookingInfo->getPnzCode() . ' ' .
                                'has been deleted from pick up history.'
                            );

                            return $app->redirect(
                                $app['url_generator']->generate('USER.DELIVERY.HISTORY')
                            );

                        } else {

                            throw new AccessDeniedException();
                            
                        }

                    } else {

                        throw new AccessDeniedException();

                    }

                } else {

                    throw new AccessDeniedException();

                }
            }
        )
        ->method('POST')
        ->bind('BOX.STATUS.DELETEPICKUP');

        $controllers->match(
            '/box-send-for-pick-up/{id}',
            function (Application $app, $id) use ($app) {

                $session = unserialize(
                    $app['session']->get('_security_secured_area')
                );

                if ($session
                    && $session->getUser()->getId()
                    && !$app['security']->isGranted('ROLE_ADMIN')
                ) {

                    $booking = new Booking($app);
                    $bookingInfo = $booking->findByPnzCode($id);

                    if ($bookingInfo->getIsPickedUp() == 2 &&
                        $bookingInfo->getUserId() == $session->getUser()->getId()
                    ) {

                        $bookingInfo->updateBookingStatusById($isPickedUp = 0);

                        $cache = $app['doctrine.cache'];
                        if (!$smsSessionId = $cache->fetch($app['cache']['key'])) {
                            $smsSessionId = $app['sms.request']
                                ->getCommand('Auth')
                                ->execute()
                                ->getSessionId();
                            $cache->save(
                                $app['cache']['key'],
                                $smsSessionId,
                                $app['cache']['lifetime']
                            );
                        }

                        $app['smsSessionId'] = $smsSessionId;
                        $smsMobile = $app['clickatell']['CLICKATELL_MOBILE'];
                        $smsMessage = 'New box for pick up with code '
                            . strtoupper($bookingInfo->getPnzCode())
                            . ' and with pick up date of ' . $bookingInfo->getPickUpDate() . '.';
                        $app['sms.request']
                            ->getCommand('SendMsg', array(
                                    'to' => $smsMobile,
                                    'text' => $smsMessage,
                                    'session_id' => $app['smsSessionId']
                                )
                            )
                            ->execute();

                        $smsLogs = new SmsLogs($app);
                        $smsLogs->setSmsMessage($smsMessage);
                        $smsLogs->setMobile($smsMobile);
                        $smsLogs->logSmsMessage();

                        $app['session']->getFlashBag()->set(
                            'alert',
                            'Box with code ' . $bookingInfo->getPnzCode() . ' ' .
                            'has been set for pick up.'
                        );

                        $generalInfo = new GeneralInfo($app);
                        $info = $generalInfo->findById(1);
                        $generalInfo->updateGeneralInfoById(
                            array(
                                'total_bookings' =>
                                    $info->getTotalBookings() + 1
                            ),
                            array(
                                'id' => $info->getId()
                            )
                        );

                        return $app->redirect(
                            $app['url_generator']->generate('USER.DELIVERY.HISTORY')
                        );

                    } else {

                        throw new AccessDeniedException();

                    }

                } else {

                    throw new AccessDeniedException();

                }
            }
        )
        ->method('POST')
        ->bind('BOX.STATUS.SENDPICKUP');

        return $controllers;
    }
}
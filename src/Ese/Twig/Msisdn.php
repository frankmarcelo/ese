<?php
/**
 * File: Msisdn.php
 *
 * PHP version 5.4
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Twig;

use Ese\Utils\Msisdn as MsisdnUtils;
/*
 * Class Msisdn
 * This script is the main routing interface to bootstrap
 *
 * Class Msisdn
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Msisdn extends \Twig_Extension
{
    /**
     * @return string
     */
    public function getName() {
        return "msisdn";
    }

    /**
     * @return array
     */
    public function getFilters() {
        return array(
            "msisdn" => new \Twig_Filter_Method($this, "msisdn"),
        );
    }

    /**
     * @param $mobile
     * @return mixed|string
     */
    public function msisdn($mobile) {

        return MsisdnUtils::convert($mobile);
    }
}
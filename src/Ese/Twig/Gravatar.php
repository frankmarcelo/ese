<?php
/**
 * File: Gravatar.php
 *
 * PHP version 5.4
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Twig;

/*
 * Class Gravatar
 * This script is the main routing interface to bootstrap
 *
 * Class Gravatar
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Gravatar extends \Twig_Extension
{
    /**
     * @return string
     */
    public function getName() {
        return "gravatar";
    }

    /**
     * @return array
     */
    public function getFilters() {
        return array(
            "gravatar" => new \Twig_Filter_Method($this, "gravatar"),
        );
    }

    /**
     * @param $email
     * @param int $size
     * @return string
     */
    public function gravatar($email, $size = 80) {
        return '//www.gravatar.com/avatar/' . md5(strtolower(trim($email))) . '?s=' . $size . '&d=identicon';
    }
}
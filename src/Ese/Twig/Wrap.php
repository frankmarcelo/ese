<?php
/**
 * File: Wrap.php
 *
 * PHP version 5.4
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Twig;

/*
 * Class Wrap
 * This script is the main routing interface to bootstrap
 *
 * Class Wrap
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Wrap extends \Twig_Extension
{
    /**
     * @return string
     */
    public function getName() {
        return "wrap";
    }

    /**
     * @return array
     */
    public function getFilters() {
        return array(
            "wrap" => new \Twig_Filter_Method($this, "wrap"),
        );
    }

    /**
     * @param $wordWrap
     * @return mixed
     */
    public function wrap($wordWrap) {

       return wordwrap($wordWrap, 30, "<br>");
    }
}
<?php
/**
 * File: Elasped.php
 *
 * PHP version 5.4
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Twig;

use DateTime;

/*
 * Class Elasped
 * This script is the main routing interface to bootstrap
 *
 * Class Elasped
 *
 * @category Twig
 * @package  Ese\Twig
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class Elasped extends \Twig_Extension
{
    /**
     * @return string
     */
    public function getName() {
        return "elasped";
    }

    /**
     * @return array
     */
    public function getFilters() {
        return array(
            "elasped" => new \Twig_Filter_Method($this, "elasped"),
        );
    }

    /**
     * @param $dateTime
     * @param bool $full
     * @return string
     */
    public function elasped($dateTime, $full = false) {
        $now = new DateTime();
        $ago = new DateTime(date("Y-m-d H:i:s", $dateTime));
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
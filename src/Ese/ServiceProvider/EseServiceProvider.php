<?php
/**
 * File: EseServiceProvider.php
 *
 * PHP version 5.4
 *
 * @category ServiceProvider
 * @package  Ese\ServiceProvider
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\ServiceProvider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Doctrine\Common\Cache\PhpFileCache;

/*
 * Class EseServiceProvider
 * This script is the main routing interface to bootstrap
 *
 * Class EseServiceProvider
 *
 * @category ServiceProvider
 * @package  Ese\ServiceProvider
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class EseServiceProvider implements ServiceProviderInterface
{
    /**
     * Register the global provider using pimple.
     *
     * @param Application $app bootstrap Silex app
     *
     * @return void
     */
    public function register(Application $app)
    {
        $app['doctrine.cache'] = $app->share(
            function () use ($app) {
                return new PhpFileCache($app['cache']['directory']);
            }
        );
    }

    /**
     * Bootstrap the application service provider
     *
     * @param Application $app bootstrap Silex app
     *
     * @return void
     */
    public function boot(Application $app)
    {
    }
}
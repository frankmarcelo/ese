<?php
/**
 * File: User.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  Users.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */
namespace Ese\Users;

use SimpleUser\User as BaseUser;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use Nietonfir\Google\ReCaptcha\ReCaptcha;
use Nietonfir\Google\ReCaptcha\Api\RequestData;
use Nietonfir\Google\ReCaptcha\Api\Response;

/**
 * Class User
 * This script is the main routing interface to bootstrap
 *
 * Class User
 *
 * @category Users
 * @package  Ese\Users
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class User extends BaseUser
{
    /**
     * @param string $email
     */
    public function __construct($email)
    {
        parent::__construct($email);
    }

    /**
     * @param $email
     * @param int $size
     * @return string
     */
    public function getGravatarUrl($email, $size = 80)
    {
        return '//www.gravatar.com/avatar/' .
            md5(strtolower(trim($email))) .
            '?s=' . $size . '&d=identicon';
    }

    /**
     * @return array
     */
    public function validate()
    {
        $errors = parent::validate();

        $request = Request::createFromGlobals();
        if ($request->request->has('mobile')) {
            $this->setMobile($request->request->get('mobile'));
        }

        if ($request->request->has('address')) {
            $this->setAddress($request->request->get('address'));
        }

        if ($request->request->has('suburb')) {
            $this->setSuburb($request->request->get('suburb'));
        }

        if ($request->request->has('geocode_x')) {
            $this->setGeocodeX($request->request->get('geocode_x'));
        }

        if ($request->request->has('geocode_y')) {
            $this->setGeocodeY($request->request->get('geocode_y'));
        }

        if (!$this->getAddress()) {
            $errors['address'] = 'Address is required.';
        }

        if (
            !$this->getGeoCodeX() ||
            !$this->getGeoCodeY() ||
            !$this->getSuburb()
        ) {
            $errors['address'] = 'Address is not found, ' .
                'select address from list of suggestion';
        }

        if (!$this->getMobile()) {
            $errors['mobile'] = 'Mobile is required.';
        }

        if (!$request->request->has('id')){
            $requestData = new RequestData(
                '6LehYv8SAAAAAAzLBQSC53OHrPofoC1R71THqidL',
                $request->request->get('g-recaptcha-response'),
                $request->server->get('REMOTE_ADDR')
            );

            $reCaptcha = new ReCaptcha(new Client(), new Response());
            $reCaptcha->processRequest($requestData);
            if (!$reCaptcha->getResponse()->isValid()) {
                $errors['recaptcha'] = 'Invalid anti spam code.';
            }
        }

        return $errors;
    }
}
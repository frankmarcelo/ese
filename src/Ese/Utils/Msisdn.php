<?php
/**
 * File: Msisdn.php
 *
 * PHP version 5.4
 *
 * @category Utils
 * @package  Ese\Utils
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

namespace Ese\Utils;

use Silex\Application;

/**
 * Class Msisdn
 * This script is the main routing interface to bootstrap
 * instead being called upon on routing.php.
 * All http requests will come to this routing definitions
 * otherwise if not found it will render a not found.
 *
 * Class Msisdn
 * @package  Ese\Utils
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 * @codeCoverageIgnore
 */
class Msisdn
{
    /**
     * @param $msisdn
     * @return mixed|string
     */
    public static function convert($msisdn)
    {
        $msisdn = str_replace(array(" ", "-"), array("", ""),trim($msisdn));
        if (preg_match("/^02/", $msisdn)) {
            $msisdn = preg_replace('/02/', '+642', $msisdn, 1);
        } elseif (preg_match("/^64/", $msisdn)) {
            $msisdn = '+' . $msisdn;
        }
        return $msisdn;
    }

    /**
     * @param $msisdn
     * @return mixed|string
     */
    public static function convertPhilMobile($msisdn) {
        $msisdn = str_replace(array(" ", "-"), array("", ""),trim($msisdn));
        return $msisdn;
    }
}
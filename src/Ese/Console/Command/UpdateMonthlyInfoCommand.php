<?php
/**
 * File: UpdateMonthlyInfoCommand.php
 *
 * PHP version 5.4
 *
 * @category Command
 * @package  UpdateMonthlyInfoCommand.php
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
namespace Ese\Console\Command;

use Ese\Models\Booking;
use Ese\Models\MonthlyGeneralInfo;
use Silex\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

/**
 * Class UpdateMonthlyInfoCommand
 * This script is the main routing interface to bootstrap
 *
 * Class UpdateMonthlyInfoCommand
 *
 * @category Command
 * @package  Ese\Console\Command
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */
class UpdateMonthlyInfoCommand extends Command
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('UpdateMonthlyInfo:update')
            ->setDescription('Update monthly information');
    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $monthlyGeneration = new MonthlyGeneralInfo($this->app);
        $bookings = new Booking($this->app);
        $bookingsData = $bookings->findAllBookingByMonth(0,1);
        $bookingsConfirmed = $bookings->findAllBookingByMonth(1,1);


        $bookingsAmount = 0;
        foreach ($bookingsConfirmed as $booking)
        {
            $bookingsAmount += $booking->getAmount();
        }

        $style = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);

        if ($monthlyGenerationInfo = $monthlyGeneration->findByInfoYearMonth()) {

            $params = array(
                'overall_booking' => count($bookingsData),
                'overall_amount' => $bookingsAmount
            );

            $monthlyGenerationInfo->updateGeneralInfoById($params, array('id' => $monthlyGenerationInfo->getId()));

        } else {

            $monthlyGeneration->setInfoYearMonth(date("Y-m") . '-01');
            $monthlyGeneration->setOverallAmount($bookingsAmount);
            $monthlyGeneration->setOverallBooking(count($bookingsData));
            $monthlyGeneration->insertMonthlyGeneralInfoBy();

        }
        $output->writeln('<fg=black;bg=cyan>finished.</fg=black;bg=cyan>');

    }
}
<?php

ini_set('display_errors', 0);
require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

require __DIR__ . '/../app/kernel.php';
require __DIR__ . '/../app/routing.php';

$app->run();

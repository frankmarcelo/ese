<?php
/**
 * File: index_dev.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  index_dev.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 *
 * This check prevents access to debug front controllers that are deployed by accident to production servers.
 * Feel free to remove this, extend it, or make something more sophisticated.
 *
 */
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1', '107.15.88.251'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once __DIR__.'/../vendor/autoload.php';

Symfony\Component\Debug\Debug::enable();

$app = new Silex\Application();

require __DIR__ . '/../app/kernel.php';
require __DIR__ . '/../app/routing.php';

$app->run();
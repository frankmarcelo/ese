<?php
/**
 * File: BootstrapSilexApplication.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  BootstrapSilexApplication.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */

namespace tests;

use Silex\WebTestCase;
use Silex\Application;

class BootstrapSilexApplication extends WebTestCase
{
    /**
     * @var string
     */
    protected $lmsCookies = null;

    /**
     * @var $app 
     */
    protected $app;

    /**
     * Initialise the construct function for creating a silex unit test
     */
    public function __construct()
    {
        $this->createApplication();
    }

    /**
     * Bootstrap the Silex micro framework, it includes the kernel and route providers
     *
     * @return Application|\Symfony\Component\HttpKernel\HttpKernel
     */
    public function createApplication()
    {
        $app = new Application();

        require __DIR__ . '/../app/kernel.php';
        require __DIR__ . '/../app/routing.php';

        $app['debug'] = true;
        $app['exception_handler']->disable();

        return $this->app = $app;
    }
}
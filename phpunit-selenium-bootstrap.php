<?php
/**
 * File: phpunit-selenium-bootstrap.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  phpunit-selenium-bootstrap.php
 * @author   Franklin Marcelo <frankitoy@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://bitbucket.org/frankitoy/ese
 */

require_once __DIR__ . '/vendor/autoload.php';

PHPUnit_Extensions_Selenium2TestCase::shareSession(true);